#pragma once

namespace GameModel
{
  class IGame
  {
  public:
    virtual void Update() = 0;
    virtual inline float GetDeltaTime() = 0;
  };
}