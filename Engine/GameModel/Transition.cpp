#include "Transition.h"

using namespace GameModel;

Transition::Transition(GameObject& obj, glm::vec3 targetPos, bool rotate, Action* action) :
  obj(&obj), srcP(obj.position), srcR(obj.rotation), targetP(targetPos), action(action), rotate(rotate)
{
  obj.moving = true;
  if (targetPos.x < obj.position.x)
    targetR = srcR + (glm::pi<float>() / 2);
  else
    targetR = srcR - (glm::pi<float>() / 2);
}

/*
* Call each frame to animate an objects transition
* Returns true when the transition is finished, false otherwise.
*/
bool Transition::Update()
{

  if (finished) return true;

  t += pIGame->GetDeltaTime() * Globals::MOVE_SPEED;
  obj->position = srcP + t * (targetP - srcP);
  if (rotate)
    obj->rotation = srcR + t * (targetR - srcR);

  // If transition is over
  if (t >= 1.0f)
  {
    obj->position = targetP;
    if(rotate)
      obj->rotation = targetR;

    obj->moving = false;

    // Perform post action passed as functor
    if (action != nullptr)
    {
      (*action)();
      delete action;
    }

    // Flag transition for deletion
    finished = true;
    return true;
  }

  // Return false if transition is still going
  return false;

}