#include "LevelTitle.h"

using namespace GameModel;

LevelTitle::LevelTitle(int levelNo) : Level(levelNo)
{
  uiObj = new TitleObject(0, Location(-1, -1), pIRenderer->titleShader, glm::vec3(0.0f), glm::vec3(23.8f, 14.0f, 1.0f));
  uiObj->active = false;
  uiObj->Initialize();

  shine1 = new TitleObject(1, Location(-1, -1), pIRenderer->sparkleShader, glm::vec3(-1.54f, -0.7f, 0.5f), glm::vec3(1.0f, 1.0f, 1.0f));
  shine1->Initialize();
  shine1->rotation = 90;

  shine2 = new TitleObject(2, Location(-1, -1), pIRenderer->sparkleShader, glm::vec3(0.51f, -0.39f, 0.5f), glm::vec3(1.0f, 1.0f, 1.0f));
  shine2->Initialize();

  enter = new TitleObject(3, Location(-1, -1), pIRenderer->enterShader, glm::vec3(1.425f, -0.7f, 0.5f), glm::vec3(2.0f, 2.0f, 1.0f));
  enter->Initialize();

}

// Update carried out in the Game Class
int LevelTitle::Update()
{
  if(Globals::playerControls.startGame)
  {
    // Set this value to be 0 to display the levelSelect Screen
    return 0;
  }

  shine1->rotation -= 2.5f * pIGame->GetDeltaTime();
  shine2->rotation -= pIGame->GetDeltaTime();

  if (enter->scale.x > 2.2f)
    scaleFlip = true;
  if (enter->scale.x < 1.8f)
    scaleFlip = false;

  if (scaleFlip)
    enter->scale -= 1 * pIGame->GetDeltaTime();
  else
    enter->scale += 1 * pIGame->GetDeltaTime();

  return -1;
}