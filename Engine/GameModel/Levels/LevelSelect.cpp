#include "LevelSelect.h"
#include "../GameObjects/Utility Objects/Pointer.h"
#include "../GameObjects/Utility Objects/TitleObject.h"

using namespace GameModel;

LevelSelect::LevelSelect(int _levelNo, LevelData * _levelData) : Level(levelNo), LevelDataArray(_levelData)
{
  // Block keypressed when levelselect first loads
  key_timer = -0.5f;
  key_cooldown = true;

  // Reset the startGame variable to avoid skipping this Screen
  Globals::playerControls.startGame = false;
  Globals::playerControls.space = false;

  AddHelpText();

  InitLvlText();
  //InitPointer();

  uiObj = new TitleObject(0, Location(-1, -1), pIRenderer->lvlSelectShader, glm::vec3(0.0f), glm::vec3(23.8f, 14.0f, 1.0f));
  uiObj->active = false;
  uiObj->Initialize();

  Display(_levelData);
}

// Update carried out in the Game Class
int LevelSelect::Update()
{
  updateNeeded = false;

  // A cooldown timer to 
  if (key_timer > key_cooldownTime)
  {
    key_cooldown = false;
    //PrintPointerValues();
  }

  if (Globals::playerControls.space && !key_cooldown)
  {
    // Set the flag for the game to swap the level arrays
    Globals::levelSettings.reloadLevels = true;

    if (DEBUG.report)
      std::cout << "Switching Level Files" << std::endl;

    updateNeeded = true;
    key_cooldown = true;
    key_timer = 0.0f;
  }

  // Scroll through the level List
  if (Globals::playerControls.up && !key_cooldown)
  {
    MoveUpList();
  }
  else if (Globals::playerControls.down && !key_cooldown)
  {
    MoveDownList();
  }
  else if (Globals::playerControls.left && !key_cooldown)
  {
    MoveUpList(10);
  }
  else if (Globals::playerControls.right && !key_cooldown)
  {
    MoveDownList(10);
  }

  if (updateNeeded)
  {
    updateNeeded = false;
    UpdateLevelList(LevelDataArray);
  }

  if (Globals::playerControls.startGame)
  {
    // Return the desired levelNo here to load the correct level within the
    // Game classes update method
    return selectedLevel;
  }

  // Increase time to the cooldown timer
  key_timer += pIGame->GetDeltaTime();
  return -1;
}

/*
 * When moving up the list up one spot, we will need to move the highlighted
 * position, as well as the starting position
 */
void LevelSelect::MoveUpList()
{
  selectedLevel--;
  if (highlightedPos == 1)
  {
    if (selectedLevel <= 0)
    {
      highlightedPos = MAXLEVELSTODISPLAY;
      if (Globals::levelSettings.loadNewLevels)
      {
        startPos = Globals::TOTALNEWLEVELS - MAXLEVELSTODISPLAY;
      }
      else
      {
        startPos = Globals::TOTALOLDLEVELS - MAXLEVELSTODISPLAY;
      }
    }
    else
    {
      startPos--;
    }
  }
  else
  {
    highlightedPos--;
  }

  if (selectedLevel <= 0)
  {
    if (Globals::levelSettings.loadNewLevels)
    {
      selectedLevel = Globals::TOTALNEWLEVELS;
    }
    else
    {
      selectedLevel = Globals::TOTALOLDLEVELS;
    }
  }

  if (DEBUG.report)
    std::cout << "HighlightedPos = " << highlightedPos << std::endl;

  if (DEBUG.report)
    std::cout << "Selected Level = " << selectedLevel << std::endl;
  key_cooldown = true;
  key_timer = 0.0f;

  updateNeeded = true;
}

/*
 * When moving up the list up MORE THAN one spot, we WONT need to move the
 * highlighted position, but we will need to move the starting position
 */
void LevelSelect::MoveUpList(int lvlsToMove)
{

  if (selectedLevel > lvlsToMove)
  {
    // Move up in list selected no of levels
    selectedLevel -= lvlsToMove;
    if (selectedLevel < startPos + 1)
    {
      startPos = selectedLevel - 1;
      highlightedPos = 1;
    }

    if (DEBUG.report)
      std::cout << "Selected Level = " << selectedLevel << std::endl;
    if (DEBUG.report)
      std::cout << "StartPos = " << startPos << std::endl;

  } else
  {
    selectedLevel = 1;
    highlightedPos = 1;
    startPos = 0;
  }

  key_cooldown = true;
  key_timer = 0.0f;

  updateNeeded = true;

}

void LevelSelect::MoveDownList()
{

  selectedLevel++;
  if (highlightedPos == MAXLEVELSTODISPLAY)
  {
    if ((Globals::levelSettings.loadNewLevels && selectedLevel > Globals::TOTALNEWLEVELS) ||
      (!Globals::levelSettings.loadNewLevels && selectedLevel > Globals::TOTALOLDLEVELS))
    {
      highlightedPos = 1;
      startPos = 0;
    }
    else
    {
      startPos++;
    }
  }
  else
  {
    highlightedPos++;
  }

  if ((Globals::levelSettings.loadNewLevels && selectedLevel > Globals::TOTALNEWLEVELS) ||
    (!Globals::levelSettings.loadNewLevels && selectedLevel > Globals::TOTALOLDLEVELS))
  {
    selectedLevel = 1;
  }

  updateNeeded = true;
  key_cooldown = true;
  key_timer = 0.0f;

}

void LevelSelect::MoveDownList(int lvlsToMove)
{

  int maxLevel;

  if (Globals::levelSettings.loadNewLevels)
    maxLevel = Globals::TOTALNEWLEVELS;
  else
    maxLevel = Globals::TOTALOLDLEVELS;

  if (selectedLevel + lvlsToMove < maxLevel && startPos + lvlsToMove <= maxLevel - MAXLEVELSTODISPLAY)
  {
    selectedLevel += lvlsToMove;
    startPos += lvlsToMove;

    if (DEBUG.report)
    {
      std::cout << "StartPos = " << startPos << std::endl;
      std::cout << "HighlightedPos = " << highlightedPos << std::endl;
      std::cout << "Selected Level = " << selectedLevel << std::endl;
    }

  } else
  {
    selectedLevel = maxLevel;
    highlightedPos = MAXLEVELSTODISPLAY;
    startPos = maxLevel - MAXLEVELSTODISPLAY;
  }

  updateNeeded = true;
  key_cooldown = true;
  key_timer = 0.0f;

}

/*
* Function to render the text objects onto the screen.
* The display works from bottom left to top right, with the min and max screen
* size stored under   int Globals::WIN_WIDTH  and  int Globals::WIN_HEIGHT
*                       (w,h)
*      ----------------x
*      |               |
*      |               |
*      |               |
*      |               |
*      |               |
*      |               |
*      x----------------
* (0,0)
*/
void LevelSelect::Display(LevelData * _levelData)
{
  std::string newOrOld;
  int lvlsCompleted = 0;
  int totalLevels;
  glm::vec2 pos;

  // Shortuct variables to allow quick access to these globals
  int w = Globals::WIN_WIDTH;
  int h = Globals::WIN_HEIGHT;

  // Check if we are loading the new or old levels, and adjust all varaibles
  // asscociated with this
  if (Globals::levelSettings.loadNewLevels)
  {
    newOrOld = "GEMENON ";
    lvlsCompleted = Globals::levelSettings.newLevelsCompleted;
    totalLevels = Globals::TOTALNEWLEVELS;
    pos = glm::vec2(w - 590, h - 30);
  }
  else
  {
    newOrOld = "SUPAPLEX ";
    lvlsCompleted = Globals::levelSettings.oldLevelsCompleted;
    totalLevels = Globals::TOTALOLDLEVELS;
    pos = glm::vec2(w - 600, h - 30);
  }

  // ---===--- TOP OF SCREEN ---===---
  textObjects.push_back(new TextObject(0, newOrOld + "LEVEL SELECT", 0.7f, WHITE, pos));

  // Display levels
  UpdateLevelList(_levelData);

  // Calculate percentage of levels completed
  float percent = (float)lvlsCompleted / (float)totalLevels * 100;

  // Generate text for the Levels Completed
  ss.str("");
  ss << "LEVELS COMPLETED  ";
  ss << std::setfill(' ') << std::setw(3) << lvlsCompleted;
  ss << "  /  " << std::setfill(' ') << std::setw(3) << totalLevels << "    ";
  ss << std::fixed << std::setw(3) << std::setprecision(1) << std::setfill(' ') << percent << " % COMPLETE";
  text = ss.str();
  // Display Levels Completed text
  textObjects.push_back(new TextObject(0, text, 0.6f, WHITE, glm::vec2(0.2*w, 10)));
  // ---===--- BOTTOM OF SCREEN ---===---
}

void LevelSelect::UpdateLevelList(LevelData * _levelData)
{
  // Level Data to display
  int _levelNo = 0;
  std::string levelName;
  bool completed = false;

  // Display levels avaiable to load
  for (int pos = 0; pos < MAXLEVELSTODISPLAY; pos++)
  {
    _levelNo = _levelData[startPos + pos].levelNo;
    levelName = _levelData[startPos + pos].levelName;
    completed = _levelData[startPos + pos].completed;

    if (DEBUG.report)
      std::cout << "POS = " << pos << "   LevelNo = " << _levelNo << "    LevelName = " << levelName << std::endl;
    leveltxtList.at(pos)->text = CreateLevelInfoString(_levelNo, levelName);

    if (completed)
      leveltxtList.at(pos)->setColour(RED);
    else
      leveltxtList.at(pos)->setColour(BLUE);

    if (selectedLevel == _levelNo)
      leveltxtList.at(pos)->setColour(WHITE);
  }
}

// Function to concatonate the levelNo and Levelname into a string
std::string LevelSelect::CreateLevelInfoString(int _levelNo, std::string levelName)
{
  ss.str("");  // Clear String Stream
  ss << " " << std::setfill('0') << std::setw(3) << _levelNo << " " << levelName;
  return ss.str();
}

void LevelSelect::InitLvlText()
{
  // Variable to track the height to draw the next text object
  int currHeight = STARTHEIGHT;

  for (int pos = 0; pos < MAXLEVELSTODISPLAY; pos++)
  {
    leveltxtList.push_back(new TextObject(0, " ", 1.0f, BLUE, glm::vec2(TEXTWIDTH, currHeight)));
    textObjects.push_back(leveltxtList.at(pos));
    currHeight = currHeight - TEXTHEIGHT;
  }
}

void LevelSelect::InitPointer()
{
  //pointerObj = new Pointer(0, Location(STARTHEIGHT, TEXTWIDTH - 20));
  pointerObj = new Pointer(0, Location(0, 0), glm::vec3(0, 0, 0));
  //pointerObj = new Pointer(0, Location::ToWorld())
  objects.push_back(pointerObj);
  pointerObj->Initialize();
}

void LevelSelect::PrintPointerValues()
{
  //if (DEBUG.report)
  //{ 
  std::cout << "POSITION x = " << pointerObj->position.x;
  std::cout << "  y = " << pointerObj->position.y;
  std::cout << "  z = " << pointerObj->position.z << std::endl;
  //}
}

void LevelSelect::AddHelpText()
{
  //float startingHeight = Globals::WIN_HEIGHT - 200;
  //float textwidth = 40;

  textObjects.push_back(new TextObject(0, "UP/DOWN/LEFT/RIGHT: Move", 0.5f, WHITE,
    glm::vec2(40, Globals::WIN_HEIGHT - 140)));

  textObjects.push_back(new TextObject(0, "ESC: Return to Level Select", 0.5f, WHITE,
    glm::vec2(40, Globals::WIN_HEIGHT - 170)));

  textObjects.push_back(new TextObject(0, "Space: Toggle Level Mode", 0.5f, RED,
    glm::vec2(40, Globals::WIN_HEIGHT - 230)));
  
  textObjects.push_back(new TextObject(0, "Alt+Enter: Toggle Fullscreen", 0.5f, WHITE,
    glm::vec2(40, Globals::WIN_HEIGHT - 260)));

  textObjects.push_back(new TextObject(0, "Q: Quit to Desktop", 0.5f, WHITE,
    glm::vec2(40, Globals::WIN_HEIGHT - 290)));

  //textObjects.push_back(new TextObject(0, "switch between", 1.0f, RED,
  //  glm::vec2(textwidth, startingHeight - (2 * TEXTHEIGHT))));

  //textObjects.push_back(new TextObject(0, " NEW and OLD", 1.0f, RED,
  //  glm::vec2(textwidth, startingHeight - (3 * TEXTHEIGHT))));

  //textObjects.push_back(new TextObject(0, "    levels ", 1.0f, RED,
  //  glm::vec2(textwidth, startingHeight - (4 * TEXTHEIGHT))));
}