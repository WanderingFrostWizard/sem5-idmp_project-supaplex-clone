#pragma once
#include "Level.h"
#include "../GameObjects/Utility Objects/TitleObject.h"

namespace GameModel
{

  class LevelTitle : public Level
  {
  private:

    TitleObject * shine1 = nullptr;
    TitleObject * shine2 = nullptr;
    TitleObject * enter = nullptr;
    bool scaleFlip = false;

  public:

    LevelTitle(int levelNo);
    virtual ~LevelTitle()
    {
      pIRenderer->UnregisterUI(shine1);
      pIRenderer->UnregisterUI(shine2);
      pIRenderer->UnregisterUI(enter);
      delete shine1;
      delete shine2;
      delete enter;
    }

    virtual int Update() override;

  };

}