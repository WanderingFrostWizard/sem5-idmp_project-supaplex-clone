#pragma once

#include <vector>
#include <string>
#include <sstream>
#include <iomanip>
#include "../../Includes.h"
#include "../Transition.h"
#include "../Levels/LevelData.h"
#include "../GameObjects/Dirt.h"
#include "../GameObjects/DarkDirt.h"
#include "../GameObjects/Crystal.h"
#include "../GameObjects/Rock.h"
#include "../GameObjects/BreakableWall.h"
#include "../GameObjects/Boulder.h"
#include "../GameObjects/Player.h"
#include "../GameObjects/Exit.h"
#include "../GameObjects/Utility Objects/Explosion.h"
#include "../GameObjects/Utility Objects/TextObject.h"
#include "../GameObjects/Enemies/Enemy.h"

namespace GameModel
{
  class Level
  {
  public:
    int nextId = 0;
    int levelNo = 0;
    int infotronTarget = 0;
    int width, height;
    GameObject* player = nullptr;
    bool lvlComplete = false;

    // String stream to concatonate strings
    std::stringstream ss;
    std::string text;

    // Font colours
    const glm::vec3 WHITE = glm::vec3(1.0f , 1.0f, 1.0f);
    const glm::vec3 BLUE  = glm::vec3(0.25f, 0.5f, 1.0f);
    const glm::vec3 RED   = glm::vec3(1.0f , 0.0f, 0.0f);

    // Reference to the textobject responsible for printing the remaining 
    // Crystals to the screen, to allow it to be updated
    // This is ONLY used within LevelStandard, however I was unable to
    // Reference it from Level::RequestMove(), unless I defined it here
    TextObject * crystalText;

    /*
    * Store all level objects here.
    * Consider these pointer the primary pointers for the objects.
    */
    GameObject * uiObj;
    std::vector<GameObject*> objects;
    std::vector<GameObject*> explosions;
    std::vector<Location*> expQueue;
    std::vector<TextObject*> textObjects;
    std::vector<Transition> transitions;

     /*
     * Board Datastructure
     * This is an array of Element Pointers that HAS YET TO BE INITIALIZED with
     * a width and height. This will be done in the constructor.
     * The GameObject pointers stored here can be moved around and deleted without any issue,
     * as the object vector above stores them all in a raw format for memory management purposes.
     */
    GameObject *** board;

    Level(int levelNo);
    virtual ~Level()
    {
      // Delete all registered objects
      for (auto object : objects)
      {
        pIRenderer->UnregisterObject(object);
        delete object;
      }

      // Delete all registered explosions
      for (auto object : explosions)
      {
        pIRenderer->UnregisterObject(object);
        delete object;
      }

      pIRenderer->UnregisterUI(uiObj);
      delete uiObj;

      // Delete all registered text objects
      for (auto object : textObjects)
      {
        delete object;
      }

      // Delete the board array
      for (int y = 0; y < height; ++y)
        delete[] board[y];
      delete[] board;

    }

    GameObject*& RetrieveLocation(const Location& loc);
    bool CheckLocation(const Location& loc);
    bool RemoveLocationGood(const Location& loc);
    static void RemoveObject(GameObject* obj);
    static void RemoveAndKillObject(GameObject* obj);
    static void RemoveLocation(Location& loc);
    void CreateExplosion(Location loc);
    void CreateCrystals(Location loc);

    virtual int Update() = 0;
    
    int getInfotronTarget()
    {
      return infotronTarget;
    }
    void pickupInfotron()
    {
      if (infotronTarget > 0 )
        infotronTarget--;
      UpdateCrystals();
    }

    // Movement functions
    bool RequestMove(Location& from, Location& to, bool rotate = false);

    /*
     * This function is used to update the Remaining crystal textbox in 
     * LevelStandard.  Sadly, I cannot reference this from within 
     * Level::RequestMove, unless it is here, even if it really should be in
     * the LevelStandard class
     */
    void UpdateCrystals();
  };
  extern Level* loadedLevel;
}