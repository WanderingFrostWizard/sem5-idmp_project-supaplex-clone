#include "LevelStandard.h"
#include "../GameObjects/Utility Objects/TitleObject.h"
using namespace GameModel;
 
LevelStandard::LevelStandard(int levelNo, LevelData * _currLevelData) : Level(levelNo)
{
  // Assign the current level data to the Level class variables
  height = _currLevelData->height;
  width = _currLevelData->width;

  // Assign the LevelStandard Class variables
  levelName = _currLevelData->levelName;
  infotronTarget = _currLevelData->target;
 
  initBoard( );

  // Create background plane
  bg = new TitleObject(0, Location(-1, -1), pIRenderer->backgroundShader, glm::vec3(0.0f, 0.0f, -0.8f), glm::vec3(23.8f, 14.0f, 1.0f));
  bg->Initialize();

  // Create background dirt
  generateBackground();

  // Load data into the board array
  generateBoard(_currLevelData->board);

  CreateGUI();

  if (DEBUG.report)
    ConsoleBoard();
}

// Update carried out in the Game Class
int LevelStandard::Update()
{
  // Update all objects
  for (auto object : objects)
  {
    if (object->active) object->Update();
  }

  // Update explosions
  std::vector<GameObject*>::iterator i = explosions.begin();
  while (i != explosions.end())
  {
    if ((*i)->active)
    {
      (*i)->Update();
      ++i;
      // TODO: Error occurs here when chaining exp
    }
    else
    {
      pIRenderer->UnregisterObject(*i);
      delete *i;
      i = explosions.erase(i);
    }
  }

  // Create new explosions in queue
  for ( auto loc : expQueue)
  {
    loadedLevel->CreateExplosion(*loc);
    delete loc;
    pICamera->Shake();
  }
  expQueue.clear();

  // Update all transitions
  for (int i = 0; i < transitions.size(); ++i)
  {
    // Delete the transition if its completed
    if (transitions.at(i).Update())
    {
      transitions.erase(transitions.begin() + i);
      --i;
    }
  }

  // Update camera with players location
  pICamera->FollowPlayer(player->position.x, player->position.y);

  if (Globals::playerControls.exitLevel)
  {
    // Reset this to be false (if triggered by code NOT by keypress)
    Globals::playerControls.exitLevel = false;
    //CreateExplosion(player->loc);     // THIS WILL TRIGGER THE EXPLOSION, however the next line will exit the level
                                        // Instantly, and you will NOT see the explosion
    return 0;
  }

  // If player has died, return to level select
  if(!player->active)
  {
    exit_timer += pIGame->GetDeltaTime();
    if( exit_timer > exit_delay )
      return 0;
  }

  return -1;
}

/*
* ##########################################################################
* ###########################  PRIVATE FUNCTIONS ###########################
* ##########################################################################
*/
/**
* Initializes the board array of element pointers from the given height
* and width with nullptrs
*/
void LevelStandard::initBoard( )
{
  board = new GameObject **[height];
  for (int y = 0; y < height; ++y)
    board[y] = new GameObject *[width];

  for (int y = 0; y < height; y++)
  {
    for (int x = 0; x < width; x++)
    {
      board[y][x] = nullptr;
    }
  }
}

/*
 * This function is used to create the gameobjects in their correct locations
 * This is done by reading through the char array boardFile, and then load
 * each object one by one
 */
void LevelStandard::generateBoard( char ** boardFile )
{
  char c;
  GameObject * obj = nullptr;

  // Temporarily store objects in order to initialize them based on their priority
  std::vector<GameObject*> earlyRegister;
  std::vector<GameObject*> lateRegister;

  if (DEBUG.report)
    std::cout << "Level Loaded - Array from file" << std::endl;

  for (int y = 0; y < height; y++)
  {
    // Display the current line for debugging of large arrays
    if (DEBUG.report)
      std::cout << "Line " << std::setfill(' ') << std::setw(3) << y << " ";

    for (int x = 0; x < width; x++)
    {
      Location pos = Location(x, y);

      c = boardFile[y][x];
      if (DEBUG.report)
        std::cout << c;  // Output the current character
      switch (c)
      {
      case 'B':
        obj = new Bomb(nextId, pos, Location::ToWorld(pos, width, height));
        lateRegister.push_back(obj);
        break;
      // Enemies
      case '@':
        obj = new Enemy(nextId, pos, Location::ToWorld(pos, width, height));
        lateRegister.push_back(obj);
        break;
      case 'T':
        obj = new Thief(nextId, pos, Location::ToWorld(pos, width, height));
        lateRegister.push_back(obj);
        break;

      // Walls
      case 'O':
        obj = new Rock(nextId, pos, Location::ToWorld(pos, width, height));
        earlyRegister.push_back(obj);
        break;
      case 'C':
        obj = new BreakableWall(nextId, pos, Location::ToWorld(pos, width, height));
        earlyRegister.push_back(obj);
        break;

      case '#':
        obj = new Dirt(nextId, pos, Location::ToWorld(pos, width, height));
        earlyRegister.push_back(obj);
        break;
      case 'X':
        // Player must be defined and kept as separate variable to ensure that it is initialized last for transparancy purposes
        obj = nullptr;
        player = new Player(nextId, pos, Location::ToWorld(pos, width, height, glm::vec3(0, 0, 0.05f)));
        objects.push_back(player);
        board[y][x] = player;
        break;
      case '$':
        obj = new Crystal(nextId, pos, Location::ToWorld(pos, width, height));
        earlyRegister.push_back(obj);
        break;
      case '*':
        obj = new Boulder(nextId, pos, Location::ToWorld(pos, width, height, glm::vec3(0, 0, 0.025f)));
        lateRegister.push_back(obj);
        break;
      case 'E':
        obj = new Exit(nextId, pos, Location::ToWorld(pos, width, height));
        earlyRegister.push_back(obj);
        break;
      default:
        obj = nullptr;
        break;
      }
      if (obj != nullptr)
      {
        board[y][x] = obj;
        objects.push_back(obj);
      }
      ++nextId;
    }
    if (DEBUG.report)
      std::cout << std::endl;
  }

  // Initialize objects based on their priority
  for (std::vector<GameObject*>::iterator i = earlyRegister.begin(); i != earlyRegister.end(); ++i)
    (*i)->Initialize();

  for (std::vector<GameObject*>::iterator i = lateRegister.begin(); i != lateRegister.end(); ++i)
    (*i)->Initialize();

  // Initialize player last for transparency purposes
  player->Initialize();
  pICamera->ResetCamera(player->position.x, player->position.y);
}

void LevelStandard::generateBackground()
{
  // Now create the objects in the level
  GameObject * obj = nullptr;

  for (int y = 0; y < height; ++y)
  {
    for (int x = 0; x < width; ++x)
    {
      // Create Other Objects
      Location pos = Location(x, y);
      obj = new DarkDirt(nextId, pos, Location::ToWorld(pos, width, height, glm::vec3(0, 0, -0.05f)));
      objects.push_back(obj);
      obj->Initialize();

      ++nextId;
    }
  }
}

/*
 * Used to print the current state of the board array to the console
 */
void LevelStandard::ConsoleBoard()
{
  char symbol;
  std::cout << "---===Level Loaded as game objects===---" << std::endl;

  for (int y = 0; y < height; ++y) 
  {
    //std::cout << "|";
    for (int x = 0; x < width; ++x)
    {
      if (board[y][x] == nullptr)
      {
        symbol = ' ';
      }
      else
      {
        symbol = board[y][x]->dispChar();
      }
      //std::cout << " " << symbol << " |";
      std::cout << symbol;
    }
    std::cout << std::endl;
  }
  std::cout << std::endl;
}

void LevelStandard::CreateGUI()
{
  uiObj = new TitleObject(0, Location(-1, -1), pIRenderer->uiShader, glm::vec3(0.77f, 0.0f, 1.0f), glm::vec3(13.5f, 13.5f, 1.0f));
  uiObj->active = false;
  uiObj->Initialize();

  // Display number of Crystals Remaining
  crystalText = new TextObject(0, " ", 0.7f, BLUE, glm::vec2(Globals::WIN_WIDTH - 85.0f, Globals::WIN_HEIGHT - 55.0f));
  textObjects.push_back(crystalText);

  UpdateCrystals();

  // Display Level No and Name
  ss.str("");
  ss << " " << std::setfill('0') << std::setw(3) << levelNo << "      " << levelName;
  text = ss.str();
  textObjects.push_back(new TextObject(0, text, 0.7f, BLUE, glm::vec2(Globals::WIN_WIDTH * 0.51f, 20)));
  
}