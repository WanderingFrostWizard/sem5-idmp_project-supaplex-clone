#pragma once
#include "Level.h"
#include "../GameObjects/Utility Objects/TitleObject.h"

namespace GameModel
{
  class LevelStandard : public Level
  {
  public:

    TitleObject * bg = nullptr;

    LevelStandard(int levelNo, LevelData * _currLevelData );
    virtual ~LevelStandard() {
    
      pIRenderer->UnregisterUI(bg);
      delete bg;

    }

    virtual int Update() override;

  private:
    // Level Variables
    std::string levelName = "";

    float exit_timer = 0.0f;
    const float exit_delay = 3.0f;

    void initBoard();
    void generateBoard( char ** boardFile );
    void generateBackground();
    void ConsoleBoard();  // Displays Board to console for Debugging
    void CreateGUI( );
  };
}