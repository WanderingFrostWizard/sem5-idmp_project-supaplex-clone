#pragma once
#include "Level.h"

namespace GameModel
{
  class LevelSelect : public Level
  {
  public:
    LevelSelect( int _levelNo, LevelData * _levelData );
    virtual ~LevelSelect() {};
    virtual int Update() override;

  private:
    bool key_cooldown = false;
    float key_timer = 0.0f;
    const float key_cooldownTime = 0.1f;
    bool updateNeeded;

    LevelData * LevelDataArray;
    std::vector<TextObject*> leveltxtList;
    GameObject * pointerObj = nullptr;

    // Level display variables
    const int MAXLEVELSTODISPLAY = 10;
    // Height of a line of characters in levelList
    const int TEXTHEIGHT = 60;

    const int STARTHEIGHT = Globals::WIN_HEIGHT - 100;
    const int TEXTWIDTH = 0.35 * Globals::WIN_WIDTH;
    int selectedLevel = 1;
    int highlightedPos = 1;

    // Level currently at the TOP of the list (position in array, NOT levelNo)
    int startPos = 0;

    void MoveUpList();
    void MoveUpList(int lvlsToMove);
    void MoveDownList();
    void MoveDownList(int lvlsToMove);


    void Display(LevelData * _levelData);
    void UpdateLevelList(LevelData * _levelData);
    std::string CreateLevelInfoString(int levelNo, std::string levelName);
    void InitLvlText();    
    void InitPointer();
    void PrintPointerValues();
    void AddHelpText();
  };
}