#include "Level.h"

using namespace GameModel;

Level::Level(int _levelNo)
{
  levelNo = _levelNo;
}

GameObject*& Level::RetrieveLocation(const Location& loc)
{
  return board[loc.y][loc.x];
}

/*
* This function removes the pointer in the location provided, deactivates the object in that space
* and unregisters it with the renderer.
* Note: Will not free any memory or delete the object instance itself.
* Memory will be freed upon destruction of level. However, the object will no longer update or render.
*/
bool Level::RemoveLocationGood(const Location& loc)
{
  // Just remove pointer from the board and unregister it with renderer.
  // the memory will be freed when unloading the level.
  if (board[loc.y][loc.x] != nullptr)
  {
    pIRenderer->UnregisterObject(board[loc.y][loc.x]);
    board[loc.y][loc.x]->active = false;
    board[loc.y][loc.x] = nullptr;
    return true;
  }
  else
  {
    return false;
  }
}

void Level::RemoveObject(GameObject* obj)
{
  // Unregister object with renderer.
  // the memory will be freed when unloading the level.
  if (obj != nullptr)
  {
    pIRenderer->UnregisterObject(obj);
    obj->active = false;
  }
}

void Level::RemoveLocation(Location& loc)
{
  loadedLevel->board[loc.y][loc.x] = nullptr;
}

bool Level::CheckLocation(const Location& loc)
{
  // Check this location exists in the board
  if ((loc.x >= 0 && loc.x < width) && (loc.y >= 0 && loc.y < height))
    return true;
  return false;
}

bool Level::RequestMove(Location& from, Location& to, bool rotate)
{
  if (CheckLocation(from) && CheckLocation(to))
  {
    GameObject* fromObj = board[from.y][from.x];
    GameObject* toObj = board[to.y][to.x];
    bool validMove = false;
    bool killTo = false;
    bool killFrom = false;

    if (fromObj != nullptr)
    {
      // If an object moves into the player
      if (toObj != nullptr && toObj->type == ObjType::PLAYER && fromObj->momentum
        && (fromObj->type == ObjType::BOULDER || fromObj->type == ObjType::CRYSTAL || fromObj->type == ObjType::ENEMY))
      {
        killTo = true;
      }

      // If an object moves into an enemy
      if (toObj != nullptr && toObj->type == ObjType::ENEMY && fromObj->momentum
        && (fromObj->type == ObjType::BOULDER || fromObj->type == ObjType::CRYSTAL))
      {
        killTo = true;
      }

      if (toObj != nullptr && toObj->type == ObjType::ENEMY && fromObj->type == ObjType::PLAYER)
      {
        std::cout << "HIT\n";
        killFrom = true;
      }

      // If a bomb falls on anything
      if (toObj != nullptr && fromObj->momentum
        && (fromObj->type == ObjType::BOMB))
      {
        killFrom = true;
      }

      // If a valid move
      if (toObj == nullptr || (fromObj->type == ObjType::PLAYER && toObj->edible))
      {
        // If moving to a crystal position, add to the count.
        if (toObj != nullptr && toObj->type == ObjType::CRYSTAL)
          pickupInfotron();

        // Move pointers around on the board
        board[to.y][to.x] = fromObj;

        // If moving object is the player, don't remove pointer from old location
        // (Will do that in a transition post action later)
        if (fromObj->type != ObjType::PLAYER)
          board[from.y][from.x] = nullptr;

        // Convert target board location into world space vector
        glm::vec3 target = Location::ToWorld(to, width, height, glm::vec3(0, 0, fromObj->position.z));

        // If object in destination is edible
        if (toObj != nullptr && toObj->edible)
        {
          // Create an action to delete it and pass to the transition
          Action* action;
          // If moving object is player, also add an action to remove pointer from old location when transition is finished
          if (fromObj->type == ObjType::PLAYER)
            action = new Action(toObj, &Level::RemoveObject, fromObj->loc, &Level::RemoveLocation);
          else
            action = new Action(toObj, &Level::RemoveObject);
          toObj->active = false;
          transitions.push_back(Transition(*fromObj, target, rotate, action));
        }
        else
        {
          // Otherwise create transition without a post action
          if (fromObj->type == ObjType::PLAYER)
          {
            // Or with a post action that removes player's old location if object is the player
            Action * action = new Action(nullptr, nullptr, fromObj->loc, &Level::RemoveLocation);
            transitions.push_back(Transition(*fromObj, target, rotate, action));
          }
          else
          {
            transitions.push_back(Transition(*fromObj, target, rotate));
          }
        }
        fromObj->loc = to;
        validMove = true;
        fromObj->momentum = true;
      }
      else
      {
        fromObj->momentum = false;
      }

      if (killTo)
      {
        RemoveAndKillObject(toObj);
      }

      if (killFrom)
      {
        RemoveAndKillObject(fromObj);
      }

      return validMove;
    }
  }
  return false;
}

void Level::CreateExplosion(Location loc)
{
  Explosion * exp = nullptr;
  float startTime = 0.2f;

  for (int x = loc.x - 1; x < loc.x + 2; ++x)
  {
    for (int y = loc.y - 1; y < loc.y + 2; ++y)
    {
      if (x == loc.x && y == loc.y)
        startTime = 0.0f;
      else if (x != loc.x && y != loc.y)
        startTime = 0.2f;
      else
        startTime = 0.1f;

      Location newLoc = Location(x, y);

      if (CheckLocation(newLoc))
      {
        GameObject * obj = RetrieveLocation(newLoc);

        Action * delayedDestroy = nullptr;
        if(obj != nullptr && obj->destructable)
          delayedDestroy = new Action(obj, &Level::RemoveAndKillObject);

        exp = new Explosion(nextId, startTime, newLoc, delayedDestroy,
          Location::ToWorld(newLoc, width, height, glm::vec3(0, 0, 0.05f)));

        exp->Initialize();
        explosions.push_back(exp);
        ++nextId;
      }
    }
  }
}

void Level::CreateCrystals(Location loc)
{
  Crystal * cryst = nullptr;
  float startTime = 0.2f;

  for (int x = loc.x - 1; x < loc.x + 2; ++x)
  {
    for (int y = loc.y - 1; y < loc.y + 2; ++y)
    {
      if (x == loc.x && y == loc.y)
        startTime = 0.0f;
      else if (x != loc.x && y != loc.y)
        startTime = 0.2f;
      else
        startTime = 0.1f;

      Location newLoc = Location(x, y);

      if (CheckLocation(newLoc))
      {
        GameObject * obj = RetrieveLocation(newLoc);

        if(obj == nullptr || obj->destructable)
        {
          RemoveLocationGood(newLoc);
          cryst = new Crystal(nextId, newLoc, Location::ToWorld(newLoc, width, height, glm::vec3(0, 0, 0.05f)));
          board[newLoc.y][newLoc.x] = cryst;

          cryst->Initialize();
          objects.push_back(cryst);
          ++nextId;
        }

      }
    }
  }
}

void Level::RemoveAndKillObject(GameObject* obj)
{
  // Unregister object with renderer and call it's kill function
  // the memory will be freed when unloading the level.
  if (obj != nullptr)
  {

    pIRenderer->UnregisterObject(obj);
    RemoveLocation(obj->loc);
    obj->Kill();
    obj->active = false;
  }
}

void Level::UpdateCrystals()
{
  ss.str("");
  ss << std::setfill('0') << std::setw(3) << infotronTarget;
  text = ss.str();
  crystalText->text = text;

  if (infotronTarget == 0)
    crystalText->setColour( RED );
}