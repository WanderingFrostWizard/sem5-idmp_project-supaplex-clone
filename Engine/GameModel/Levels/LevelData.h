#pragma once

#include "../../Includes.h"

namespace GameModel
{
  struct LevelData
  {
    int levelNo = 0;
    std::string levelName = "";

    // Array sizes
    int width = 0;
    int height = 0;

    // No of Crystals to collect
    int target = 0; 

    // Has this level been completed ???
    // NOTE this is NOT part of the levels.dat or NewLevels.dat
    //     IT from separate files
    bool completed;

    char ** board;

    virtual ~LevelData()
    {

      // Delete the board array
      for (int y = 0; y < height; ++y)
        delete[] board[y];
      delete[] board;
    
    }

    void initBoard()
    {
      board = new char *[height];
      for (int y = 0; y < height; ++y)
        board[y] = new char [width];

      for (int y = 0; y < height; y++)
      {
        for (int x = 0; x < width; x++)
        {
          board[y][x] = ' ';
        }
      }
    }
  };
}