#pragma once
#include "../Includes.h"
#include "Levels/LevelStandard.h"
#include "Levels/LevelTitle.h"
#include "Levels/LevelSelect.h"
#include <vector>

#include <fstream>          // For file reading
#include <sstream>          // FOR std::istringstream

namespace GameModel
{
  class Game : public IGame
  {
  public:
    Game() {}
    virtual ~Game() 
    {
      SaveGame();
    }

    void Initialize();
    void CalcTime();
    virtual void Update() override sealed;
    virtual inline float GetDeltaTime() override sealed { return dT; }

  private:
    float dT = 0.0f;
    // String stream to concatonate strings
    std::stringstream ss;

    // Array reference to acces the CURRENTLY loaded set of levels
    LevelData * levelData;

    // Array to store each set of levels when loaded from file
    LevelData * oldLevelData;
    LevelData * newLevelData;

    // File Reading methods
    void LoadFromFile();
    bool ReadSave(std::ifstream * saveIn);
    void SaveGame();
    void ReCountLevelsCompleted();
    
    // NEW LEVEL FUNCTIONS
    void ReadNewLevels(std::ifstream * levelsIn, std::ifstream * saveIn);
    void ReadLine(std::ifstream * in, std::string * token);
    void ReadNewLvlArray(std::ifstream * in, char** board, int width, int height);

    // OLD LEVEL FUNCTIONS
    void ReadOldLevels(std::ifstream * levelsIn, std::ifstream * saveIn);
    void ReadOldLvlArray(std::ifstream * in, char** board, int width, int height);
    char HexToChar(int hex);
    void ReadOldLevelData(std::ifstream * in, int _levelNo);

    // BOTH LEVEL VERSION FUNCTIONS
    void CheckFileStatus(std::ios * fileIn);
    std::string ReadLvlName(std::ifstream * in);
  };
}