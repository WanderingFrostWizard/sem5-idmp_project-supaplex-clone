#include "Game.h"

using namespace GameModel;

Level* GameModel::loadedLevel = nullptr;

void Game::Initialize()
{
  LoadFromFile();
  loadedLevel = new LevelTitle( 0 );
  ReCountLevelsCompleted();
} 

void Game::CalcTime()
{
  static int tLast = -1;

  if (tLast < 0)
    tLast = SDL_GetTicks();

  int t = SDL_GetTicks();
  dT = (float)(t - tLast) / 1000.0f;

  tLast = t;
}

void Game::Update()
{
  CalcTime();
  int levelNo = loadedLevel->Update();

  // A return of -1 here means keep the game going,
  // 0 returns to the level select screen
  // 1 or above means change level
  if(levelNo >= 0 )
  {
    // If the loaded level was completed successfully
    // NOTE this variable is reset when the level is deleted within this function
    if (loadedLevel->lvlComplete)
    {
      if (DEBUG.report)
        std::cout << "Level Completed Successfully" << std::endl;
     
      levelData[loadedLevel->levelNo - 1].completed = true;
      
      /*
       * Need to recount, all levels, as it is possible for a player to 
       * complete the same level more than once, hence a simple x++ would 
       * cause errors
       */
      ReCountLevelsCompleted();
    }

    // Remove Loaded Level
    delete loadedLevel;
    // Reset Camera
    pICamera->ResetCamera(0,0);
    // Load the next level
    // Minus 1 for the array as it is on 0->x  So to load level x, we need to 
    // load position x - 1 in the array
    if(levelNo > 0)
      loadedLevel = new LevelStandard(levelNo, &levelData[levelNo - 1]);
    else
      loadedLevel = new LevelSelect( 0, levelData );
  }
  
  // If player wants to change between loading New Levels and Old Levels
  // This was selected on the levelSelect screen using a key binding
  if (Globals::levelSettings.reloadLevels)
  {
    Globals::levelSettings.reloadLevels = false;

    if (Globals::levelSettings.loadNewLevels)
    {
      // Change to old levels
      levelData = oldLevelData;
    }
    else 
    {
      // Change to NEW levels
      levelData = newLevelData;
    }
    delete loadedLevel;
    Globals::levelSettings.loadNewLevels = !Globals::levelSettings.loadNewLevels;
    loadedLevel = new LevelSelect(0, levelData);
  }

  pIRenderer->RequestRender();
}

/*
* This function loads the level data from the appropriate levels file, then
* loads it into the board array.
*/
void Game::LoadFromFile()
{
  std::ifstream * levelsIn;
  std::ifstream * saveIn;   

  saveIn = new std::ifstream(Globals::SAVEFILELOCATION);
  CheckFileStatus(saveIn);          
 
  // Load New Levels
  if (DEBUG.report)
    std::cout << "LOADING NEW LEVELS" << std::endl;
  levelsIn = new std::ifstream(Globals::NEWLEVELFILELOCATION);
  CheckFileStatus(levelsIn);
  newLevelData = new LevelData[Globals::TOTALNEWLEVELS];
  ReadNewLevels(levelsIn, saveIn);

  levelsIn->close();

  // Read seperator in the SaveFile ';'
  saveIn->get();

  // Load OLD Levels
  if (DEBUG.report)
    std::cout << "LOADING OLD LEVELS" << std::endl;
  levelsIn = new std::ifstream(Globals::OLDLEVELFILELOCATION, std::ios::in | std::ios::binary);
  CheckFileStatus(levelsIn);
  oldLevelData = new LevelData[Globals::TOTALOLDLEVELS];
  ReadOldLevels(levelsIn, saveIn);

  // Close the file
  levelsIn->close();
  saveIn->close();
  saveIn->clear();

  // Set the levelData to point to one of the LevelData sets
  levelData = newLevelData;
}

/*
* Need to recount, all levels, as it is possible for a player to
* complete the same level more than once, hence a simple x++ would
* cause errors
*/
void Game::ReCountLevelsCompleted()
{
  int count = 0;

  if (DEBUG.report)
    std::cout << "Old = " << Globals::levelSettings.oldLevelsCompleted
    << " new = " << Globals::levelSettings.newLevelsCompleted << std::endl;

  // Check OLD Levels
  for (int levelNo = 0; levelNo < Globals::TOTALOLDLEVELS; levelNo++)
  {
    if (oldLevelData[levelNo].completed)
    {
      ++count;
      if (DEBUG.report)
        std::cout << oldLevelData[levelNo].levelNo << " " << oldLevelData[levelNo].levelName << std::endl;
    }
  }
  // Update the count for the OLD levels
  Globals::levelSettings.oldLevelsCompleted = count;

  count = 0;

  if (DEBUG.report)
    std::cout << " START of NEW LEVELS " << std::endl;
  // --------------------------------------------------------------------------
  // Check NEW Levels
  for (int levelNo = 0; levelNo < Globals::TOTALNEWLEVELS; levelNo++)
  {
    if (newLevelData[levelNo].completed)
    {
      ++count;
      if (DEBUG.report)
        std::cout << newLevelData[levelNo].levelNo << " " << newLevelData[levelNo].levelName << std::endl;
    }
  }

  // Update the count for the NEW levels
  Globals::levelSettings.newLevelsCompleted = count;

  if (DEBUG.report)
    std::cout << "Old = " << Globals::levelSettings.oldLevelsCompleted
    << " new = " << Globals::levelSettings.newLevelsCompleted << std::endl;
}

/*
 * Read in a single character from the SaveGame File
 * This function is called from ReadNewLevels and ReadOldLevels, it is called
 * immediately after a level is loaded.  
 * LOOP
 *     Load single level   (from levels file)
 *     ReadSave   (from saveGame file)
 */
bool Game::ReadSave(std::ifstream * saveIn)
{
  // Read in the relevant saveData for this level
  int value = saveIn->get();

  // Is equal to 'T'
  if (value == 0x54)
  {
    if (DEBUG.report)
      std::cout << "TRUE" << " ";
    return true;
  }
  // Is equal to 'F'
  else if (value == 0x46)
  {
    if (DEBUG.report)
      std::cout << "FALSE" << " ";
    return false;
  }
}

/*
 * Write to the SaveGame file, which is in the format
 * NEWLEVELS ; OLDLEVELS
 * FFFFFFFFFF;FFFFFFFFFFFFF
 * NOTE semicolon is used as a seperator
 * 'T' (TRUE) for completed level, or 'F' (FALSE) for uncompleted
 * NOTE the number of characters read in is based upon the global variables
 * Globals::TOTALNEWLEVELS and Globals::TOTALOLDLEVELS
 */
void Game::SaveGame()
{
  // Open file for writing
  std::ofstream saveOut;
  saveOut.open(Globals::SAVEFILELOCATION, std::ofstream::out | std::ofstream::trunc);

  CheckFileStatus(&saveOut);

  std::cout << "Saving Game" << std::endl;

  // Write Savefile data for NEW Levels
  for (int levelNo = 0; levelNo < Globals::TOTALNEWLEVELS; levelNo++)
  {
    if (newLevelData[levelNo].completed)
      saveOut << 'T';
    else
      saveOut << 'F';
  }

  // Add Seperator
  saveOut << ';';

  // Write Savefile data for OLD Levels
  for (int levelNo = 0; levelNo < Globals::TOTALOLDLEVELS; levelNo++)
  {
    if (oldLevelData[levelNo].completed)
      saveOut << 'T';
    else
      saveOut << 'F';
  }
  saveOut.close();
}

/*
* ##########################################################################
* #######################  NEW LEVEL FILE FUNCTIONS ########################
* ##########################################################################
*/
/*
 * This function reads each level from the text file, into the struct array
 */
void Game::ReadNewLevels(std::ifstream * levelsIn, std::ifstream * saveIn)
{
  std::string * token = new std::string[4];

  //while ( !in->eof() && (currLevel < Globals::TOTALNEWLEVELS) )
  for (int levelNo = 0; levelNo < Globals::TOTALNEWLEVELS; levelNo++)
  {    
    ReadLine(levelsIn, token);
    
    newLevelData[levelNo].levelNo = atoi(token[0].c_str());
    newLevelData[levelNo].width = atoi(token[1].c_str());
    newLevelData[levelNo].height = atoi(token[2].c_str());
    newLevelData[levelNo].target = atoi(token[3].c_str());
    
    newLevelData[levelNo].levelName = ReadLvlName(levelsIn);
    
    newLevelData[levelNo].initBoard();

    ReadNewLvlArray(levelsIn, newLevelData[levelNo].board, newLevelData[levelNo].width, newLevelData[levelNo].height);

    newLevelData[levelNo].completed = ReadSave(saveIn);
  }
}

// Read a single line from file, and tokenize it. Return the tokens
void Game::ReadLine(std::ifstream * in, std::string * token )
{
  std::string line;

  std::getline( *in, line);
  std::istringstream tokenizer(line);

  // Display RAW Level data
  if (DEBUG.report)
    std::cout << line << "\n";

  // Tokenize the line
  std::getline(tokenizer, token[0], ';');
  std::getline(tokenizer, token[1], ';');
  std::getline(tokenizer, token[2], ';');
  // last token: gets the remainder of the line.
  std::getline(tokenizer, token[3]);

  // If the tokenizer wasn't able to tokenize enough data
  if ( !tokenizer )
  {
    // There were and incorrect number of separators in the line
    std::cout << "ERROR incorrect file format" << std::endl;
  }
}

/*
* This function reads in the level layout, which is stored in a textfile
* This is interpreted into a char board array, which is stored as part of
* the levelData datastructure. This is done for each level in the textfile
* Later when the level is created in LevelStandard, one of these arrays is
* converted into a separate array of gameObjects
* NOTE there are too many differences to combine ReadNewLevelArray 
* and ReadOldLevelArray() into one function
*/
void Game::ReadNewLvlArray(std::ifstream * in, char** _board, int width, int height)
{
  char c;

  for (int y = 0; y < height; y++)
  {
    // Display the current line for debugging of large arrays
    if (DEBUG.report)
      std::cout << "Line " << std::setfill(' ') << std::setw(3) << y << " ";

    for (int x = 0; x < width; x++)
    {
      in->get(c);
      if (DEBUG.report)
        std::cout << c;  // Output the current character
      _board[y][x] = c;
    }
    // Catch the EOL character
    in->get(c);
    if (DEBUG.report)
      std::cout << c;
  }
}

/*
* ##########################################################################
* #######################  OLD LEVEL FILE FUNCTIONS ########################
* ##########################################################################
*/
void Game::ReadOldLevels(std::ifstream * levelsIn, std::ifstream * saveIn)
{
  const int OLDLVLWIDTH = 60;
  const int OLDLVLHEIGHT = 24;

  // Level Numbers are NOT stored in this Level file, so we have to track them
  // manually, by using a counter
  for (int levelNo = 0; levelNo < Globals::TOTALOLDLEVELS; levelNo++)
  {
    CheckFileStatus(levelsIn);

    oldLevelData[levelNo].levelNo = levelNo + 1;
    oldLevelData[levelNo].width = OLDLVLWIDTH;
    oldLevelData[levelNo].height = OLDLVLHEIGHT;
    
    oldLevelData[levelNo].initBoard();
    ReadOldLvlArray(levelsIn, oldLevelData[levelNo].board, OLDLVLWIDTH, OLDLVLHEIGHT);
     
    ReadOldLevelData(levelsIn, levelNo);

    oldLevelData[levelNo].completed = ReadSave(saveIn);
  }
}

/*
* This function reads in the level layout, which is stored in a binary file
* This is interpreted into a char board array, which is stored as part of
* the levelData datastructure. This is done for each level in the binary file
* Later when the level is created in LevelStandard, one of these arrays is
* converted into a separate array of gameObjects
* NOTE there are too many differences to combine ReadNewLevelArray
* and ReadOldLevelArray() into one function
*/
void Game::ReadOldLvlArray(std::ifstream * in, char** _board, int width, int height)
{
  int byte;

  for (int y = 0; y < height; y++)
  {
    // Display the current line number for debugging of large arrays
    if (DEBUG.report)
      std::cout << "Line " << std::setfill(' ') << std::setw(3) << y << " ";

    for (int x = 0; x < width; x++)
    {
      // Old levels are a long binary string, that requires interpretation
      // from int to char, before we can add it to the char board
      byte = in->get();
      _board[y][x] = HexToChar(byte);

      if (DEBUG.report)
        std::cout << _board[y][x];  // Output the current character
    }
    if (DEBUG.report)
      std::cout << std::endl;
  }
}

char Game::HexToChar(int hex)
{
  switch (hex)
  {
    case 0x00:   // EMPTY SPACE
      return ' ';
    case 0x01:   // Boulder
      return '*';
    case 0x02:   // Dirt
      return '#';
    case 0x03:   // PLAYER Object
      return 'X';
    case 0x04:   // Crystal
      return '$';
    case 0x07:   // Exit
      return 'E';

      // --- BREAKABLE WALLS ---
    case 0x05:   // 1x1 Chip
    case 0x1A: // CHIP open left to right
    case 0x1B: // CHIP open left to right
    case 0x26: // CHIP open UP / DOWN
    case 0x27: // CHIP open UP / DOWN
      return 'C';

      // --- UNBREAKABLE Blocks ---
    case 0x06: // Grey Block
    case 0x1C: // Unbreakable board
    case 0x1D: // Unreak Square Green Crystal
    case 0x1E: // Unreak Square BLUE Crystal
    case 0x1F: // Unreak Square RED Crystal
    case 0x20: // BRICK - Yellow
    case 0x21: // Unbreakable board
    case 0x22: // Unbreakable board Capacitor
    case 0x23: // Unreakable Board Resistors
    case 0x24: // Unreakable Board Resistors DOWN
    case 0x25: // Unreakable Board Resistors YELLOW
      return 'O';

      // --- DISKS ---
    case 0x08:    // Orange Disk - Falling Disk
    case 0x12: // Yellow Disk - Moveable, Terminal linked disk
    case 0x14: // Red Disk - Collectable, droppable
      return 'B';

    case 0x13: // Terminal
      return '%';

      // --- PIPES ---
    case 0x09: // PIPE >->
    case 0x0A: // PIPE DOWN ONLY
    case 0x0B: // PIPE LEFT ONLY
    case 0x0C: // PIPE UP ONLY	###
    case 0x0D: // PIPE RIGHT ONLY
    case 0x0E: // PIPE DOWN ONLY	###
    case 0x0F: // PIPE LEFT ONLY	###
    case 0x10: // PIPE UP ONLY	###
    case 0x15: // PIPE  UP AND DOWN
    case 0x16: // PIPE LEFT AND RIGHT
    case 0x17: // PIPE UP, DOWN, LEFT, RIGHT
      return 'P';

      // --- ENEMIES ---
    case 0x11: // Snik Snak
    case 0x19: // Bugged Dirt
      return '@';

    case 0x18: // Electron
      return 'T';
  }
}

void Game::ReadOldLevelData(std::ifstream * in, int _levelNo)
{
  int currByte = 0;
  // Number of bytes allocated to specific areas of each level stored in file
  const int PREFIXDATA = 6;
  const int LEVELNAMESIZE = 23;
  const int SUFFIXDATA = 65;
  int target = 0;

  // Read in first set of UNWANTED BYTES
  for (int i = 0; i < PREFIXDATA; i++)
  {
    in->get();
  }

  // Read in the Level Name
  for (int i = 0; i < LEVELNAMESIZE; i++)
  {
    currByte = in->get();
    ss << (char)currByte;
  }
  oldLevelData[_levelNo].levelName = ss.str();
  // Clear stringStream
  ss.str(" ");

  // Read in extra UNWANTED byte
  in->get();

  // Read in Crystal Target
  oldLevelData[_levelNo].target = in->get();
  
  if ( DEBUG.report )
    std::cout << oldLevelData[_levelNo].levelName << "  Target = "
          << oldLevelData[_levelNo].target << std::endl;

  // Read in final UNWANTED bytes
  for (int i = 0; i < SUFFIXDATA; i++)
  {
    in->get();
  }
}

/*
* ##########################################################################
* ######################  BOTH VERSION FILE FUNCTIONS ######################
* ##########################################################################
*/
// Check if the file was successfully opened
// NOTE use of std::ios to allow ifstream and ofstream to be checked
void Game::CheckFileStatus(std::ios * fileIn)
{
  //exit program if ifstream could not open file
  if (!fileIn)
  {
    std::cerr << "file could not be open" << std::endl;
    exit(1);
  }
}

/*
* Function to load in the level name from the text file
*/
std::string Game::ReadLvlName(std::ifstream * in)
{
  char c;
  std::string levelName;

  for (int i = 0; i < Globals::LEVELNAMELENGTH; i++)
  {
    in->get(c);
    levelName += c;
  }
  // Catch the EOL character
  in->get(c);

  return levelName;
}
