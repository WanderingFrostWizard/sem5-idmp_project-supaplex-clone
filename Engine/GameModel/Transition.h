#pragma once

#include "../Includes.h"
#include "GameObjects/GameObject.h"

namespace GameModel
{
  class Action
  {
  private:
    GameObject * obj;
    void(*func)(GameObject*);
    GameObject * obj2;
    void(*func2)(Location&);
    Location loc;

  public:

    Action(GameObject* obj, void(*action)(GameObject*), Location loc = Location(-1,-1), void(*action2)(Location&) = nullptr)
      : obj(obj), func(action), func2(action2), loc(loc) {}
    void operator()()
    {
      if(func != nullptr)
        func(obj);
      if(func2 != nullptr)
        func2(loc);
    }
  };

  class Transition
  {
  private:
    bool finished = false;
    bool rotate = false;
    GameObject * obj;
    float t = 0;
    
    glm::vec3 srcP;
    float srcR;
    glm::vec3 targetP;
    float targetR = 0.0f;

    Action* action;

  public:
    Transition(GameObject& obj, glm::vec3 targetPos, bool rotate = false, Action* action = nullptr);
    ~Transition() {}

    bool Update();
  };
}