#pragma once
#include "../Levels/Level.h"
#include "GameObject.h"

namespace GameModel
{

  class Exit : public GameModel::GameObject
  {

  public:

    Exit(int renderId, Location location, glm::vec3 position = glm::vec3(0.0f)) :
      GameObject(renderId, pIRenderer->exitShader, location, position)
    {
      symbol = 'E';
    }
    virtual ~Exit() {}

    // Functions from GameObject
    virtual void Initialize() override sealed;
    virtual void Update() override sealed;
    virtual bool Push(Direction& forceOrigin) override;

  };

}