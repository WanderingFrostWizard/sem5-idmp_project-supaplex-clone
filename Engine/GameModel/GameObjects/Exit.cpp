#include "Exit.h"

using namespace GameModel;

void Exit::Initialize()
{
  pIRenderer->RegisterObject(this);
}

void Exit::Update()
{
  if (loadedLevel->getInfotronTarget() <= 0)
  {
    shader = pIRenderer->exitOpenShader;
  }
}

bool Exit::Push(Direction& forceOrigin)
{
  if (loadedLevel->getInfotronTarget() <= 0)
  {
    // Set the exitLevel flag, to exit the level on the next update
    Globals::playerControls.exitLevel = true;
    loadedLevel->lvlComplete = true;
  }
  return false;
}