#pragma once
#include "../../Includes.h"
#include "../../GameView/RenderingObjects.h"
#include "../../Utilities/Util.h"

namespace GameModel
{

  class GameObject : public GameView::IRenderable
  {

  public:

    Shader * shader = nullptr;
    Shader * shader2 = nullptr;

    glm::vec3 position = glm::vec3(0.0f);
    glm::vec3 source = glm::vec3(0.0f);
    glm::vec3 target = glm::vec3(0.0f);
    float rotation = 0.0f;
    float rotSource = 0.0f;
    float rotTarget = 0.0f;

    Location loc;
    Location targetLoc;
    float t = 0.0f;
    Direction face = Direction::RIGHT;
    char symbol = ' ';

    bool active = true;
    bool locked = false;
    bool edible = false;
    bool moving = false;
    bool momentum = false;
    bool destructable = true;
    ObjType type = ObjType::STATIC;

    GameObject(int renderId, Shader * shader, Location location, glm::vec3 position = glm::vec3(0.0f), ObjType type = ObjType::STATIC, bool destructable = true) :
      IRenderable(true, renderId), shader(shader), loc(location), targetLoc(Location(-1, -1)), position(position), type(type), destructable(destructable) {}
    virtual ~GameObject() { }

    // Functions from IRenderable
    virtual void Render();

    // GameObject functions
    virtual void Initialize() = 0;
    virtual void Update() = 0;
    virtual bool Push(Direction& forceDir) = 0;
    virtual void Kill() {}

    char dispChar()
    {
      return symbol;
    }
  };

}