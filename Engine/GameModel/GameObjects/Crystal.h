#pragma once
#include "../Levels/Level.h"
#include "GameObject.h"

namespace GameModel
{

  class Crystal : public GameModel::GameObject
  {
  private:
    bool dirty = true;

  public:
    Crystal(int renderId, Location location, glm::vec3 position = glm::vec3(0.0f)) :
      GameObject(renderId, pIRenderer->crystalShader, location, position, ObjType::CRYSTAL)
    {
      symbol = '$';
    }
    virtual ~Crystal() {}

    // Functions from GameObject
    virtual void Initialize() override sealed;
    virtual void Update() override sealed;
    virtual bool Push(Direction& forceOrigin) override;

    bool Move(Direction movementDir);
  };
}