#include "Player.h"

using namespace GameModel;

// GameObject Functions
void Player::Initialize()
{
  pIRenderer->RegisterObject(this);
}

void Player::Kill()
{
  if (active)
    loadedLevel->expQueue.push_back(new Location(loc));
  //loadedLevel->CreateExplosion(loc);
}

void Player::Update()
{
  UpdateSprite();
  if (!moving)
  {
    if (Globals::playerControls.up)
    {
      SetTarget(Direction::UP, Location(loc.x, loc.y - 1));
    }
    else if (Globals::playerControls.down)
    {
      SetTarget(Direction::DOWN, Location(loc.x, loc.y + 1));
    }
    else if (Globals::playerControls.left)
    {
      SetTarget(Direction::LEFT, Location(loc.x - 1, loc.y));
    }
    else if (Globals::playerControls.right)
    {
      SetTarget(Direction::RIGHT, Location(loc.x + 1, loc.y));
    }
  }

}

void Player::SetTarget(Direction direction, Location newLoc)
{
  face = direction;
  if (loadedLevel->CheckLocation(newLoc))
  {
    GameObject* obj = loadedLevel->RetrieveLocation(newLoc);
    if (obj == nullptr || obj->Push(direction))
    {
      loadedLevel->RequestMove(loc, newLoc);

      if (obj != nullptr && obj->edible)
      {
        shader = pIRenderer->playerEatShader;
      }
      else
      {
        shader = pIRenderer->playerShader;
      }
    }
  }
}

void Player::UpdateSprite()
{
  if (moving)
  {
    eatTimer = 0.0f;
  }
  else
  {
    eatTimer += pIGame->GetDeltaTime();

    if (eatTimer > 0.1f)
    {
      shader = pIRenderer->playerShader;
    }
  }
}