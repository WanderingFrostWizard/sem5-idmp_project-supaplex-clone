#pragma once
#include "../Levels/Level.h"
#include "GameObject.h"

namespace GameModel
{
  class Boulder : public GameModel::GameObject
  {
  private:
    bool dirty = true;
    float pushTimer = 0.0f;

  public:
    Boulder(int renderId, Location location, glm::vec3 position = glm::vec3(0.0f)) : 
      GameObject(renderId, pIRenderer->boulderShader, location, position, ObjType::BOULDER)
    {
      symbol = '*';
      shader2 = pIRenderer->boulderCleanShader;
    }
    virtual ~Boulder() {}

    bool Move(Direction movementDir);

    // Functions from GameObject
    virtual void Initialize() override sealed;
    virtual void Update() override sealed;
    virtual bool Push(Direction& forceOrigin) override;
  };

  class Bomb : public Boulder
  {
  public:
    Bomb(int renderId, Location location, glm::vec3 position = glm::vec3(0.0f)) :
      Boulder(renderId, location, position)
    {
      symbol = 'B';
      shader = pIRenderer->bombShader;
      shader2 = pIRenderer->bombCleanShader;
      type = ObjType::BOMB;
    }
    virtual ~Bomb() {}
    void Kill() override;

  };

}