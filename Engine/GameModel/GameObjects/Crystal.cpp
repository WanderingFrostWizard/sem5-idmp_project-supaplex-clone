#include "Crystal.h"

using namespace GameModel;

void Crystal::Initialize()
{
  edible = true;
  pIRenderer->RegisterObject(this);
}

void Crystal::Update()
{

  // If stationary
  if (!moving)
  {
    // Try to fall, If obstructed do additional checks
    if (!Move(Direction::DOWN))
    {
      Location downLoc = Location(loc.x, loc.y + 1);
      // Check for a boulder or crystal directly under self
      GameObject * down = loadedLevel->RetrieveLocation(downLoc);
      if (down != nullptr && (down->type == ObjType::BOULDER || down->type == ObjType::CRYSTAL || down->type == ObjType::CASCADE))
      {
        Location leftLoc = Location(loc.x - 1, loc.y + 1);
        Location rightLoc = Location(loc.x + 1, loc.y + 1);

        GameObject* left = loadedLevel->RetrieveLocation(leftLoc);
        GameObject* right = loadedLevel->RetrieveLocation(rightLoc);

        // If either side is empty
        if (left == nullptr || right == nullptr)
        {
          // If right is occupied, go left
          if (right != nullptr)
          {
            Move(Direction::LEFT);
          }
          // If left is occupied, go right
          else if (left != nullptr)
          {
            Move(Direction::RIGHT);
          }
          // Only option left is both are occupied, so pick random direction
          else
          {
            float r = random.Random();
            if (r > 0.5f)
              Move(Direction::LEFT);
            else
              Move(Direction::RIGHT);
          }

        }

      }

    }

  }

}

bool Crystal::Push(Direction& forceOrigin)
{
  return true;
}

bool Crystal::Move(Direction movementDir)
{

  Location newLoc(-1, -1);
  bool rotate = false;
  switch (movementDir)
  {
  case Direction::UP:
    newLoc = Location(loc.x, loc.y - 1);
    break;
  case Direction::DOWN:
    newLoc = Location(loc.x, loc.y + 1);
    break;
  case Direction::LEFT:
    newLoc = Location(loc.x - 1, loc.y);
    rotate = true;
    break;
  case Direction::RIGHT:
    newLoc = Location(loc.x + 1, loc.y);
    rotate = true;
    break;
  }

  if (loadedLevel->RequestMove(loc, newLoc, rotate))
  {
    if (dirty)
    {
      dirty = false;
      shader = pIRenderer->crystalCleanShader;
    }
    return true;
  }
  else
  {
    return false;
  }

}