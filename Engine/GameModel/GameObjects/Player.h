#pragma once
#include "../../Includes.h"
#include "../Levels/Level.h"
#include "GameObject.h"

namespace GameModel
{

  class Player: public GameObject
  {
  private:

    float eatTimer = 1.0f;

  public:

    Player(int renderId, Location location, glm::vec3 position = glm::vec3(0.0f)) :
      GameObject(renderId, pIRenderer->playerShader, location, position, ObjType::PLAYER)
    {
      symbol = 'X';
    }

    // Functions from GameObject
    virtual void Initialize() override sealed;
    virtual void Update() override sealed;
    virtual bool Push(Direction& forceOrigin) override { return false; }
    virtual void Kill() override;

    // Player Functions
    void SetTarget(Direction direction, Location newLoc);
    void UpdateSprite();

  };

}