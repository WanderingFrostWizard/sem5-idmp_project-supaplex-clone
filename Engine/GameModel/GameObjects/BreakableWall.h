#pragma once
#include "Rock.h"

namespace GameModel
{
  class BreakableWall : public Rock
  {
  public:
    BreakableWall(int renderId, Location location, glm::vec3 position = glm::vec3(0.0f)) : Rock( renderId, location, position )
    {
      shader = pIRenderer->breakableWallShader;
      destructable = true;
      symbol = 'C';
      type = ObjType::CASCADE;
    }
    virtual ~BreakableWall() {}
  };
}