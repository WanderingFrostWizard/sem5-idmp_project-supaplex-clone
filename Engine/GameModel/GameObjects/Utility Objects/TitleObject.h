#pragma once
#include "../GameObject.h"

namespace GameModel
{
  class TitleObject : public GameObject
  {
  public:

    glm::vec3 scale;

    TitleObject(int renderId, Location location, Shader * shader, glm::vec3 position, glm::vec3 scale) :
      GameObject(renderId, shader, location, position), scale(scale) {}
    virtual ~TitleObject() {}

    // Functions from GameObject
    virtual void Initialize() override sealed { pIRenderer->RegisterUI(this); }
    virtual void Update() override sealed {}
    // Render method can be found in Render_GameObject.cpp
    virtual void Render() override sealed;
    virtual bool Push(Direction& forceOrigin) override { return false; }


  };

}