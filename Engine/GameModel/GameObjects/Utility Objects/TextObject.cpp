#include "TextObject.h"

using namespace GameModel;

TextObject::TextObject(int id, std::string text, float size, glm::vec3 colour, glm::vec2 position) :
  IRenderableText(id, text, size, colour.x, colour.y, colour.z, position.x, position.y)
{
  pIRenderer->RegisterText(this);
}

TextObject::~TextObject()
{
  pIRenderer->UnregisterText(this);
}

void TextObject::setColour( glm::vec3 newColour )
{
  colour[0] = newColour.x;
  colour[1] = newColour.y;
  colour[2] = newColour.z;
}