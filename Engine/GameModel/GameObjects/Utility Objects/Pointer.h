#pragma once
#include "../GameObject.h"

namespace GameModel
{
  class Pointer : public GameObject
  {
  public:
    Pointer(int renderId, Location location, glm::vec3 position = glm::vec3(0.0f)) :
      GameObject(renderId, pIRenderer->playerShader, location, position) {}
    virtual ~Pointer() {}

    // Functions from GameObject
    virtual void Initialize() override sealed { pIRenderer->RegisterObject(this); }
    virtual void Update() override sealed {}
    virtual bool Push(Direction& forceOrigin) override { return false; }
  };
}