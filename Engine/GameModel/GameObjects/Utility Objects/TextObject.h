#pragma once
#include "../../../Includes.h"

namespace GameModel
{
  class TextObject : public GameView::IRenderableText
  {
  public:
    TextObject(int id, std::string text, float size, glm::vec3 colour, glm::vec2 position);
    ~TextObject();
    void setColour( glm::vec3 newColour );
  };
}