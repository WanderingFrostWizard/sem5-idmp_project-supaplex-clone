#include "Explosion.h"
#include "../../Levels/Level.h"

using namespace GameModel;

void Explosion::Initialize()
{
  pIRenderer->RegisterObject(this);
}

void Explosion::Update()
{
  if (renderMe)
  {
    alive_time += pIGame->GetDeltaTime();
    if (alive_time > LIFETIME)
    {
      renderMe = false;
      active = false;
    }
  }
  else
  {
    timer += pIGame->GetDeltaTime();
    if (timer > startTime)
    {
      renderMe = true;
      // Perform post action passed as functor
      if (action != nullptr)
      {
        (*action)();
        delete action;
      }
    }
  }
}