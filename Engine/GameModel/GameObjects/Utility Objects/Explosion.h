#pragma once
#include "../../Transition.h"
#include "../GameObject.h"

namespace GameModel
{

  class Explosion : public GameModel::GameObject
  {
  private:

    const float LIFETIME = 1.5f;
    float startTime;
    float timer = 0.0f;
    float alive_time = 0.0f;
    Action * action = nullptr;

  public:

    Explosion(int renderId, float startTime, Location location, Action* action = nullptr, glm::vec3 position = glm::vec3(0.0f)) :
      GameObject(renderId, pIRenderer->explodeShader, location, position), startTime(startTime), action(action)
    { renderMe = false; }
    virtual ~Explosion() {}

    // Functions from GameObject
    virtual void Initialize() override sealed;
    virtual void Update() override sealed;
    virtual bool Push(Direction& forceOrigin) override { return false; }
    virtual void Kill() override sealed {};

  };

}