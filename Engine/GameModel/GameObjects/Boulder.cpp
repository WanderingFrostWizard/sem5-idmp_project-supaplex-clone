#include "Boulder.h"

using namespace GameModel;

void Boulder::Initialize()
{
  // Register object with renderer, otherwise it will not display
  pIRenderer->RegisterObject(this);

  // Setup random rotation
  int r = random.Random(4);
  switch (r)
  {
  case 1:
    rotation = glm::pi<float>() / 2.0f;
    break;
  case 2:
    rotation = glm::pi<float>();
    break;
  case 3:
    rotation = (3 * glm::pi<float>()) / 2.0f;
    break;
  }

}

void Boulder::Update()
{

  // If stationary
  if (!moving)
  {
    // Try to fall, If obstructed do additional checks
    if (!Move(Direction::DOWN))
    {
      Location downLoc = Location(loc.x, loc.y + 1);

      // Check for a boulder or crystal directly under self
      GameObject * down = loadedLevel->RetrieveLocation(downLoc);
      if (down != nullptr && (down->type == ObjType::BOULDER || down->type == ObjType::CRYSTAL || down->type == ObjType::CASCADE))
      {
        Location leftLoc = Location(loc.x - 1, loc.y + 1);
        Location rightLoc = Location(loc.x + 1, loc.y + 1);

        GameObject* left = loadedLevel->RetrieveLocation(leftLoc);
        GameObject* right = loadedLevel->RetrieveLocation(rightLoc);

        // If either side is empty
        if (left == nullptr || right == nullptr)
        {
          // If right is occupied, go left
          if (right != nullptr)
          {
            Move(Direction::LEFT);
          }
          // If left is occupied, go right
          else if (left != nullptr)
          {
            Move(Direction::RIGHT);
          }
          // Only option left is both are occupied, so pick random direction
          else
          {
            float r = random.Random();
            if (r > 0.5f)
              Move(Direction::LEFT);
            else
              Move(Direction::RIGHT);
          }

        }

      }

    }

  }

}

bool Boulder::Push(Direction& forceDir)
{
  if (!moving)
  {
    if (pushTimer > 0.5f)
    {
      pushTimer = 0.0f;

      // Ask for move
      if (Move(forceDir))
      {
        return true;
      }
      else
      {
        return false;
      }
    }
    else
    {
      pushTimer += pIGame->GetDeltaTime();
      return false;
    }
  }
  return false;
}

bool Boulder::Move(Direction movementDir)
{
  Location newLoc(-1,-1);
  bool rotate = false;
  switch (movementDir)
  {
  case Direction::UP:
    return false;
    //newLoc = Location(loc.x, loc.y - 1);
    break;
  case Direction::DOWN:
    newLoc = Location(loc.x, loc.y + 1);
    break;
  case Direction::LEFT:
    newLoc = Location(loc.x - 1, loc.y);
    rotate = true;
    break;
  case Direction::RIGHT:
    newLoc = Location(loc.x + 1, loc.y);
    rotate = true;
    break;
  }

  if(loadedLevel->RequestMove(loc, newLoc, rotate))
  {
    if (dirty)
    {
      dirty = false;
      shader = shader2;
    }
    return true;
  } else
  {
    return false;
  }
}

void Bomb::Kill()
{
  if (active)
    loadedLevel->expQueue.push_back(new Location(loc));
    //loadedLevel->CreateExplosion(loc);
}