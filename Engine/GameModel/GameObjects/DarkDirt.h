#pragma once
#include "GameObject.h"

namespace GameModel
{
  class DarkDirt : public GameModel::GameObject
  {
  public:
    DarkDirt(int renderId, Location location, glm::vec3 position = glm::vec3(0.0f)) :
      GameObject(renderId, pIRenderer->darkDirtShader, location, position)
    {
      active = false;
    }
    virtual ~DarkDirt() { }

    // Functions from GameObject
    virtual void Initialize() override sealed { pIRenderer->RegisterObject(this);  }
    virtual void Update() override sealed {};
    virtual bool Push(Direction& forceOrigin) override { return true; }
  };
}