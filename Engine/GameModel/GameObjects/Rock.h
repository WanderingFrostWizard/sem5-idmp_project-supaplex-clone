#pragma once
#include "GameObject.h"

namespace GameModel
{

  class Rock : public GameModel::GameObject
  {

  public:

    Rock(int renderId, Location location, glm::vec3 position = glm::vec3(0.0f)) : 
      GameObject(renderId, pIRenderer->rockShader, location, position, ObjType::STATIC, false)
    {
      symbol = 'O';
      active = false;
    }
    virtual ~Rock() {}

    // Functions from GameObject
    virtual void Initialize() override sealed;
    virtual void Update() override sealed { };
    virtual bool Push(Direction& forceOrigin) override;

  };

}