#pragma once
#include "../../Levels/Level.h"
#include "../GameObject.h"

namespace GameModel
{
  class Enemy : public GameModel::GameObject
  {
  private:
    bool wall = false;
    float turnTimer = 0.0f;

  public:
    Enemy(int renderId, Location location, glm::vec3 position = glm::vec3(0.0f)) : 
      GameObject(renderId, pIRenderer->enemyShader, location, position, ObjType::ENEMY)
    {
      symbol = '@';
    }
    virtual ~Enemy() {}

    bool Move(Direction movementDir);
    void CheckLeftWall();
    bool CheckWall(Location checkLoc);

    // Functions from GameObject
    virtual void Initialize() override;
    virtual void Update() override;
    virtual bool Push(Direction& forceOrigin) override;
    virtual void Kill() override;
  };

  class Thief : public GameModel::Enemy
  {
  public:
    Thief(int renderId, Location location, glm::vec3 position = glm::vec3(0.0f)) : Enemy(renderId, location, position)
    {
      shader = pIRenderer->thiefShader;
      symbol = 'T';
    }

    virtual ~Thief() {}

    virtual void Kill() override;
  };
}