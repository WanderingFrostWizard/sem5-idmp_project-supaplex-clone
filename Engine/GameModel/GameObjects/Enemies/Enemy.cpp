#include "Enemy.h"

using namespace GameModel;

void Enemy::Initialize()
{
  // Register object with renderer, otherwise it will not display
  pIRenderer->RegisterObject(this);
}

void Enemy::Kill()
{
  if (active)
    loadedLevel->expQueue.push_back(new Location(loc));
}

void Enemy::Update()
{
  if (!moving)
  {
    if (wall)
    {
      // Move the direction enemy is facing
      if (!Move(face))
      {
        // If can't move anymore, turn right
        if (turnTimer <= 0)
        {
          face = TurnRight(face);
          turnTimer = 0.1f;
        } else
        {
          turnTimer -= pIGame->GetDeltaTime();
        }
      }
    }
    else
    {
      // Check walls to left and behind
      Location checkLoc = GetLoc( GetLoc(loc, TurnRight(TurnRight(face))) , TurnLeft(face) );
      if(CheckWall(checkLoc))
      {
        face = TurnLeft(face);
        Move(face);
      } else
      {
        // Move the direction enemy is facing
        if (!Move(face))
        {
          // If can't move anymore, turn right
          face = TurnRight(face);
        }
      }
    }

    // Now check for wall in new location
    CheckLeftWall();
  }
}

void Enemy::CheckLeftWall()
{
  // Check for wall on left
  if (CheckWall(GetLoc(loc, TurnLeft(face))))
    wall = true;
  else
    wall = false;
}

bool Enemy::CheckWall(Location checkLoc)
{
  GameObject * obj = loadedLevel->RetrieveLocation(checkLoc);
  if (obj != nullptr && obj->type != ObjType::PLAYER)
    return true;
  else
    return false;
}

bool Enemy::Push(Direction& forceDir)
{
  return false;
}

bool Enemy::Move(Direction movementDir)
{
  Location newLoc = GetLoc(loc, movementDir);
  if (loadedLevel->RequestMove(loc, newLoc, false))
  {
    return true;
  }
  else
  {
    return false;
  }
}


/*
* Thief Functions
*/
void Thief::Kill()
{
  if (active)
    loadedLevel->CreateCrystals(loc);
}