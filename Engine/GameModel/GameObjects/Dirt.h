#pragma once
#include "GameObject.h"

namespace GameModel
{
  class Dirt : public GameModel::GameObject
  {
  public:
    Dirt(int renderId, Location location, glm::vec3 position = glm::vec3(0.0f)) :
      GameObject(renderId, pIRenderer->dirtShader, location, position)
    {
      symbol = '#';
      active = false;
    }
    virtual ~Dirt() {}

    // Functions from GameObject
    virtual void Initialize() override sealed
    {
      edible = true;
      pIRenderer->RegisterObject(this);
    }

    virtual void Update() override sealed {};

    virtual bool Push(Direction& forceOrigin) override
    {
      return true;
    }
  };
}