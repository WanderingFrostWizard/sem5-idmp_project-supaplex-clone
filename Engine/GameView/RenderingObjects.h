#pragma once
#include "../Includes.h"
#include "Shaders/Shaders.h"

namespace GameView
{

  struct Vertex
  {
    glm::vec3 v;
    glm::vec2 t;
  };

  struct Mesh
  {
    GLuint vertBuff = -1;
    GLuint indexBuff = -1;
    GLuint vao = -1;
    Vertex* verts;
    unsigned* indices;
    size_t numVerts, numIndices;

    Mesh() { }
    ~Mesh()
    {
      if (vertBuff != -1)
      {
        glDeleteBuffers(1, &vertBuff);
        glDeleteBuffers(1, &indexBuff);
      }
      delete[] verts;
      delete[] indices;
    }
    void BuildBuffers();

  };

}