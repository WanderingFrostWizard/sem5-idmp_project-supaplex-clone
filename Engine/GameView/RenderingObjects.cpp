#include "RenderingObjects.h"

using namespace GameView;

void Mesh::BuildBuffers()
{

  numVerts = 4;
  numIndices = 3 * 2; // 2 triangles

  verts = new Vertex[numVerts];
  indices = new unsigned[numIndices];

  float halfWidth = Globals::CELL_WIDTH / 2.0f;
  float halfHeight = Globals::CELL_HEIGHT / 2.0f;

  verts[0].v = glm::vec3(-halfWidth, -halfHeight, 0.0f);
  verts[0].t = glm::vec2(0.0f, 0.0f);

  verts[1].v = glm::vec3(halfWidth, -halfHeight, 0.0f);
  verts[1].t = glm::vec2(1.0f, 0.0f);

  verts[2].v = glm::vec3(halfWidth, halfHeight, 0.0f);
  verts[2].t = glm::vec2(1.0f, 1.0f);

  verts[3].v = glm::vec3(-halfWidth, halfHeight, 0.0f);
  verts[3].t = glm::vec2(0.0f, 1.0f);

  indices[0] = 0;
  indices[1] = 1;
  indices[2] = 2;
  indices[3] = 0;
  indices[4] = 2;
  indices[5] = 3;

  // Generate Vertex Array Object
  glGenVertexArrays(1, &vao);
  glBindVertexArray(vao);

  // Generate and store Vertex Buffer Object
  glGenBuffers(1, &vertBuff);
  glBindBuffer(GL_ARRAY_BUFFER, vertBuff);
  glBufferData(GL_ARRAY_BUFFER, numVerts * sizeof(Vertex), verts, GL_STATIC_DRAW);
  glGenBuffers(1, &indexBuff);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuff);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, numIndices * sizeof(unsigned int), indices, GL_STATIC_DRAW);

  // Store data in Vertex Array Object
  glEnableVertexAttribArray(0);
  glEnableVertexAttribArray(1);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<void*>(0));
  glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<void*>(sizeof(glm::vec3)));

  // Unbind VBO and VAO
  glDisableVertexAttribArray(0);
  glDisableVertexAttribArray(1);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);

  CHECK_GL_ERROR;

}