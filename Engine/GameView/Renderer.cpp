#include "Renderer.h"
#include <iostream>

using namespace GameView;

GameView::Mesh * GameView::mainMesh = nullptr;

Renderer::~Renderer()
{
  delete textRenderer;

  // Levels
  delete titleShader;
  delete sparkleShader;
  delete lvlSelectShader;
  delete enterShader;
  delete backgroundShader;

  // Walls
  delete rockShader;
  delete breakableWallShader;
  
  //Objects
  delete playerShader;
  delete playerEatShader;
  delete dirtShader;
  delete darkDirtShader;
  delete crystalShader;
  delete crystalCleanShader;
  delete boulderShader;
  delete boulderCleanShader;
  delete exitShader;
  delete exitOpenShader;
  delete bombShader;
  delete bombCleanShader;

  // Enemies
  delete enemyShader;
  delete thiefShader;

  // Utilities
  delete explodeShader;

  //UI
  delete uiShader;

  delete mainMesh;
  delete mainCam;

  SDL_Quit();
}

void Renderer::Initialize()
{

  mainCam = new Camera(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 0.0f), -1.0f,
    glm::radians(45.0f), 0.05f, 100.0f);
  pICamera = mainCam;

  LoadIdentityMatrices();

  if (!InitGL())
  {
    CHECK_GL_ERROR;
    throw std::runtime_error("Error initialising OpenGL, aborting program.");
  }

  glEnable(GL_CULL_FACE);

  std::cout << "Renderer: " << glGetString(GL_RENDERER) << '\n';
  std::cout << "OpenGL Version: " << glGetString(GL_VERSION) << '\n';
  std::cout << "GLSL Version: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << '\n';
  std::cout << "GLEW Version: " << glewGetString(GLEW_VERSION) << '\n';

  CHECK_GL_ERROR;

  // Levels
  titleShader = new Shader(Globals::STANDARDSHADER_VERT, Globals::STANDARDSHADER_FRAG, Globals::SPRITE_TITLE);
  sparkleShader = new Shader(Globals::STANDARDSHADER_VERT, Globals::STANDARDSHADER_FRAG, Globals::SPRITE_SPARKLE);
  lvlSelectShader = new Shader(Globals::STANDARDSHADER_VERT, Globals::STANDARDSHADER_FRAG, Globals::SPRITE_LVLSELECT);
  enterShader = new Shader(Globals::STANDARDSHADER_VERT, Globals::STANDARDSHADER_FRAG, Globals::SPRITE_ENTER);
  backgroundShader = new Shader(Globals::STANDARDSHADER_VERT, Globals::STANDARDSHADER_FRAG, Globals::SPRITE_BG);

  // Walls
  rockShader = new Shader(Globals::STANDARDSHADER_VERT, Globals::STANDARDSHADER_FRAG, Globals::SPRITE_ROCK);
  breakableWallShader = new Shader(Globals::STANDARDSHADER_VERT, Globals::STANDARDSHADER_FRAG, Globals::SPRITE_BREAKABLE_WALL);
  
  // Objects
  playerShader = new Shader(Globals::STANDARDSHADER_VERT, Globals::STANDARDSHADER_FRAG, Globals::SPRITE_PLAYER);
  playerEatShader = new Shader(Globals::STANDARDSHADER_VERT, Globals::STANDARDSHADER_FRAG, Globals::SPRITE_PLAYER_EAT);
  dirtShader = new Shader(Globals::STANDARDSHADER_VERT, Globals::STANDARDSHADER_FRAG, Globals::SPRITE_DIRT);
  darkDirtShader = new Shader(Globals::STANDARDSHADER_VERT, Globals::STANDARDSHADER_FRAG, Globals::SPRITE_DARKDIRT);
  crystalShader = new Shader(Globals::STANDARDSHADER_VERT, Globals::STANDARDSHADER_FRAG, Globals::SPRITE_CRYSTAL_DIRT);
  crystalCleanShader = new Shader(Globals::STANDARDSHADER_VERT, Globals::STANDARDSHADER_FRAG, Globals::SPRITE_CRYSTAL);
  boulderShader = new Shader(Globals::STANDARDSHADER_VERT, Globals::STANDARDSHADER_FRAG, Globals::SPRITE_BOULDER_DIRT);
  boulderCleanShader = new Shader(Globals::STANDARDSHADER_VERT, Globals::STANDARDSHADER_FRAG, Globals::SPRITE_BOULDER);
  exitShader = new Shader(Globals::STANDARDSHADER_VERT, Globals::STANDARDSHADER_FRAG, Globals::SPRITE_EXIT);
  exitOpenShader = new Shader(Globals::STANDARDSHADER_VERT, Globals::STANDARDSHADER_FRAG, Globals::SPRITE_EXIT_OPEN);
  bombShader = new Shader(Globals::STANDARDSHADER_VERT, Globals::STANDARDSHADER_FRAG, Globals::SPRITE_BOMB_DIRT);
  bombCleanShader = new Shader(Globals::STANDARDSHADER_VERT, Globals::STANDARDSHADER_FRAG, Globals::SPRITE_BOMB);

  // Enemies
  enemyShader = new Shader(Globals::STANDARDSHADER_VERT, Globals::STANDARDSHADER_FRAG, Globals::SPRITE_ENEMY);
  thiefShader = new Shader(Globals::STANDARDSHADER_VERT, Globals::STANDARDSHADER_FRAG, Globals::SPRITE_THEIF);

  // UI
  uiShader = new Shader(Globals::STANDARDSHADER_VERT, Globals::STANDARDSHADER_FRAG, Globals::SPRITE_UI);

  // Utilities
  explodeShader = new Shader(Globals::STANDARDSHADER_VERT, Globals::STANDARDSHADER_FRAG, Globals::SPRITE_EXPLODE);

  mainMesh = new Mesh();
  mainMesh->BuildBuffers();
}


bool Renderer::InitGL()
{
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);

  //SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_COMPATIBILITY);

  SDL_GLContext mainGLContext = SDL_GL_CreateContext(Globals::window.SDL_win);
  if (mainGLContext == 0) {
    return false;
  }

  if (SDL_GL_MakeCurrent(Globals::window.SDL_win, mainGLContext) != 0) {
    return false;
  }

  // Check for and clear error list before calling glewInit(),
  // as it WILL generate one
  CHECK_GL_ERROR;

  //glewExperimental = GL_TRUE;
  GLenum err = glewInit();
  if (err != GLEW_OK)
    return false;

  // Now clear and ignore the error glewInit() is guaranteed to create
  glGetError();

  glClearColor(0.0, 0.0, 0.0, 1.0);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_TEXTURE_2D);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  ReshapeWin();

  // Initialize text renderer (Must occur after ReshapeWin()!)
  textRenderer = new Text();
  if (!textRenderer->Init(&projMatrix))
  {
    std::cout << "Text Renderer failed to initialize." << '\n';
    return false;
  }

  CHECK_GL_ERROR;
  return true;
}

void Renderer::ReshapeWin(int w, int h)
{
  if (w == 0 || h == 0)
  {
    SDL_GetWindowSize(Globals::window.SDL_win, &w, &h);
  }
  else
  {
    SDL_SetWindowSize(Globals::window.SDL_win, w, h);
  }
  Globals::window.width = w;
  Globals::window.height = h;
  glViewport(0, 0, (GLsizei)w, (GLsizei)h);

  // Adjust projection
  projMatrix = mainCam->UpdatePerspective(w, h);

  CHECK_GL_ERROR;
}

void Renderer::ToggleFullscreen()
{
  if (!fullscreen)
  {
    if (SDL_SetWindowFullscreen(Globals::window.SDL_win, SDL_WINDOW_FULLSCREEN) == 0)
    {
      fullscreen = true;
      ReshapeWin();
    }
  }
  else
  {
    if (SDL_SetWindowFullscreen(Globals::window.SDL_win, 0) == 0)
    {
      fullscreen = false;
      ReshapeWin(Globals::WIN_WIDTH, Globals::WIN_HEIGHT);
    }
  }
}


inline void Renderer::UpdateMatrices(Shader& shader)
{
  if (currentShader != shader.id)
  {
    if (DEBUG.errors)
      std::cout << "Invalid Operation: UpdateMatrices().\n"
      "Currently loaded Shader does not match one specifed.\n";
    return;
  }

  glUniformMatrix4fv(shader.modelMatrixLoc, 1, false, (float *)&modelMatrix);
  glUniformMatrix4fv(shader.viewMatrixLoc, 1, false, (float *)&viewMatrix);
  glUniformMatrix4fv(shader.projMatrixLoc, 1, false, (float *)&projMatrix);

}


inline void Renderer::LoadIdentityMatrices()
{
  modelMatrix = identityMatrix;
  viewMatrix = identityMatrix;
  projMatrix = identityMatrix;
}

inline void Renderer::SetCameraLast(const float& mouseX, const float& mouseY)
{
  mainCam->SetLast(mouseX, mouseY);
}

void Renderer::RegisterObject(IRenderable* obj)
{
  objects.push_back(obj);
}

void Renderer::RegisterUI(IRenderable * obj)
{
  uiObj.push_back(obj);
}

void Renderer::UnregisterUI(IRenderable * obj)
{
  // search for id, then .erase(position)
  std::vector<IRenderable*>::iterator i = uiObj.begin();
  while (i != uiObj.end())
  {
    if ((*i)->id == obj->id)
    {
      i = uiObj.erase(i);
      break;
    }
    else
    {
      ++i;
    }
  }
}

void Renderer::UnregisterObject(IRenderable* obj)
{
  // search for id, then .erase(position)
  std::vector<IRenderable*>::iterator i = objects.begin();
  while (i != objects.end())
  {
    if ((*i)->id == obj->id)
    {
      i = objects.erase(i);
      break;
    }
    else
    {
      ++i;
    }
  }
}

void Renderer::RegisterText(IRenderableText* obj)
{
  textObjects.push_back(obj);
}

void Renderer::UnregisterText(IRenderableText* obj)
{
  // search for id, then .erase(position)
  std::vector<IRenderableText*>::iterator i = textObjects.begin();
  while (i != textObjects.end())
  {
    if ((*i)->id == obj->id)
    {
      i = textObjects.erase(i);
      break;
    }
    else
    {
      ++i;
    }
  }
}


void Renderer::Render()
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glViewport(0, 0, Globals::window.width, Globals::window.height);

  // Update view from camera
  viewMatrix = mainCam->UpdateView();

  for (auto object : objects)
  {
    if (object->renderMe)
    {
      object->Render();
    }
  }

  for (auto ui : uiObj)
  {
    if (ui->renderMe)
    {
      ui->Render();
    }
  }
  //if(uiObj != nullptr)
  //  uiObj->Render();

  for (auto text : textObjects)
  {
    if (text->renderMe)
      textRenderer->Render(text->text, text->position[0], text->position[1],
        text->size, glm::vec3(text->colour[0], text->colour[1], text->colour[2]));
  }

  SDL_GL_SwapWindow(Globals::window.SDL_win);
  m_wantRender = false;

  CHECK_GL_ERROR;
}

void Renderer::UpdateCameraRotation(const float& mouseX, const float& mouseY)
{
  mainCam->UpdateRotation(mouseX, mouseY);
}

void Renderer::UpdateCameraZoom(const float& mouseY)
{
  mainCam->UpdateZoom(mouseY);
}