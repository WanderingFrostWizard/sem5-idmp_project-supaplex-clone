#pragma once
/******************************************************************************
* Adapted from code originally written by pknowles
*
* NOTE: make sure to call glewInit before loading shaders
*
* use getShader() to load, compile shaders and return a program
* use glUseProgram(program) to activate it
* use glUseProgram(0) to return to fixed pipeline rendering
* use glDeleteProgram() to free resources
******************************************************************************/

#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <SOIL/SOIL.h>

#define CHECK_GL_ERROR if(DEBUG.errors) GLError(__LINE__, __FILE__)

int GLError(int line, const char* file);
void UseShader(GLuint shaderID);
GLuint getShader(const char* vertexFile, const char* fragmentFile);

class Shader
{
public:

  GLuint id;
  GLuint modelMatrixLoc;
  GLuint viewMatrixLoc;
  GLuint projMatrixLoc;

  GLuint tex;

  Shader(const char* vertexFile, const char* fragFile, const char* textureFile = "0");

  void loadTexture(const char* filename);

};