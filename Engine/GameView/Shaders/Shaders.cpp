#include "shaders.h"
#include "../../Utilities/Globals.h"

#ifdef _WIN32
#pragma warning(disable:4996)
#endif

GLuint currentShader = 0;

Shader::Shader(const char* vertexFile, const char* fragFile, const char* textureFile)
{
  id = getShader(vertexFile, fragFile);

  if (id != 0)
  {
    modelMatrixLoc = glGetUniformLocation(id, "modelMatrix");
    viewMatrixLoc = glGetUniformLocation(id, "viewMatrix");
    projMatrixLoc = glGetUniformLocation(id, "projMatrix");

    if(textureFile != "0")
      loadTexture(textureFile);
  }

  CHECK_GL_ERROR;

}

void Shader::loadTexture(const char* filename)
{
  tex = SOIL_load_OGL_texture(filename, SOIL_LOAD_AUTO,
    SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y);
  if (tex <= 0)
    std::cout << "Error loading file: " << filename << std::endl;

  UseShader(id);
  glUniform1i(glGetUniformLocation(id, "tex"), 0);
  UseShader(0);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

  CHECK_GL_ERROR;
}

int GLError(int line, const char* file)
{
  GLenum glErr;
  int retCode = 0;
  while ((glErr = glGetError()) != GL_NO_ERROR) {
    const char* p = strrchr(file, '\\');
    if (p > 0) file = p + 1;

    std::cout << "glError (" << glErr << ") in " << file << " at " << line << ": " << gluErrorString(glErr) << '\n';
    retCode = 1;
  }
  return retCode;
}

int shaderError(GLuint shader, const char* name)
{
  int infologLength = 0;
  int charsWritten = 0;
  int success = 0;
  GLchar *infoLog;

  CHECK_GL_ERROR;
  glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
  glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infologLength);
  CHECK_GL_ERROR;
  if (!success) {
    if (infologLength > 1) {
      infoLog = (GLchar *)malloc(infologLength);
      glGetShaderInfoLog(shader, infologLength, &charsWritten, infoLog);
      printf("Shader InfoLog (%s):\n%s", name, infoLog);
      free(infoLog);
    }
    else
      printf("Shader InfoLog (%s): <no info log>\n", name);
    return 1;
  }
  CHECK_GL_ERROR;
  return 0;
}

int programError(GLuint program, const char* vert, const char* frag)
{
  int infologLength = 0;
  int charsWritten = 0;
  int success = 0;
  GLchar *infoLog;

  CHECK_GL_ERROR;
  glGetProgramiv(program, GL_LINK_STATUS, &success);
  glGetProgramiv(program, GL_INFO_LOG_LENGTH, &infologLength);
  CHECK_GL_ERROR;
  if (!success) {
    if (infologLength > 1) {
      infoLog = (GLchar *)malloc(infologLength);
      glGetProgramInfoLog(program, infologLength, &charsWritten, infoLog);
      printf("Program InfoLog (%s/%s):\n%s", vert, frag, infoLog);
      free(infoLog);
    }
    else
      printf("Program InfoLog (%s/%s): <no info log>\n", vert, frag);
    return 1;
  }
  CHECK_GL_ERROR;
  return 0;
}

char* readFile(const char* filename)
{
  int size;
  char* data;
  FILE* file = fopen(filename, "rb");
  if (!file) return NULL;
  fseek(file, 0, 2);
  size = ftell(file);
  fseek(file, 0, 0);
  data = (char*)malloc(size+1);
  fread(data, sizeof(char), size, file);
  fclose(file);
  data[size] = '\0';
  return data;
}

void cleanupShader(GLuint vert, GLuint frag, char* vertSrc, char* fragSrc)
{
  glDeleteShader(vert);
  glDeleteShader(frag);
  free(vertSrc);
  free(fragSrc);
}

// Use this instead of glUseProgram, so I can track what shader is currently active
void UseShader(GLuint shaderID)
{
  currentShader = shaderID;
  glUseProgram(shaderID);
};

GLuint getShader(const char* vertexFile, const char* fragmentFile)
{

  char* vertSrc;
  char* fragSrc;

  CHECK_GL_ERROR;

  /* read the contents of the source files */
  vertSrc = readFile(vertexFile);
  fragSrc = readFile(fragmentFile);

  /* check they exist */
  if (!vertSrc || !fragSrc)
  {
    free(vertSrc);
    free(fragSrc);
    printf("Error reading shaders %s, %s & %s\n", vertexFile, fragmentFile);
    fflush(stdout); 
    return 0;
  }

  /* create the shaders */
  GLuint vert, frag, program;
  vert = glCreateShader(GL_VERTEX_SHADER);
  frag = glCreateShader(GL_FRAGMENT_SHADER);

  /* pass in the source code for the shaders */
  glShaderSource(vert, 1, (const GLchar**)&vertSrc, NULL);
  glShaderSource(frag, 1, (const GLchar**)&fragSrc, NULL);

  /* compile and check each for errors */
  glCompileShader(vert);
  if (shaderError(vert, vertexFile))
  {
    cleanupShader(vert, frag, vertSrc, fragSrc);
    return 0;
  }
  glCompileShader(frag);
  if (shaderError(frag, fragmentFile))
  {
    cleanupShader(vert, frag, vertSrc, fragSrc);
    return 0;
  }

  /* create program, attach shaders, link and check for errors */
  program = glCreateProgram();
  glAttachShader(program, vert);
  glAttachShader(program, frag);
  glLinkProgram(program);
  if (programError(program, vertexFile, fragmentFile))
  {
    cleanupShader(vert, frag, vertSrc, fragSrc);
    glDeleteProgram(program);
    return 0;
  }
  /* clean up intermediates and return the program */
  cleanupShader(vert, frag, vertSrc, fragSrc);

  CHECK_GL_ERROR;

  return program; /* NOTE: use glDeleteProgram to free resources */
}