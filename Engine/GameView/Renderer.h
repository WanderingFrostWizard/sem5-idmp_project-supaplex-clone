#pragma once
#include "../Includes.h"
#include "Shaders/Shaders.h"
#include "Camera.h"
#include "RenderingObjects.h"
#include "Text.h"
#include <stdexcept>
#include <vector>

namespace GameView
{

  // GameView Constants
  const glm::mat4 identityMatrix(1.0);
  extern Mesh * mainMesh;

  class Renderer : public IRenderer
  {

  public:

    Text * textRenderer = nullptr;

    glm::mat4 modelMatrix = identityMatrix;
    glm::mat4 viewMatrix = identityMatrix;
    glm::mat4 projMatrix = identityMatrix;

    Renderer() {}
    ~Renderer();

    void Initialize();
    bool InitGL();

    virtual void Render() override sealed;
    virtual void ReshapeWin(int w = 0, int h = 0) override sealed;
    virtual inline void RequestRender() override sealed { m_wantRender = true; }
    virtual inline bool WantsRender() override sealed { return m_wantRender; }

    // Renderable Object management
    virtual void RegisterObject(IRenderable* obj) override sealed;
    virtual void UnregisterObject(IRenderable* obj) override sealed;

    virtual void RegisterUI(IRenderable* obj) override sealed;
    virtual void UnregisterUI(IRenderable* obj) override sealed;

    virtual void RegisterText(IRenderableText* obj) override sealed;
    virtual void UnregisterText(IRenderableText* obj) override sealed;

    // Video Mode
    virtual void ToggleFullscreen() override sealed;

    // Matrix Manipulation
    virtual inline void UpdateMatrices(Shader& shader) override sealed;
    virtual inline void LoadIdentityMatrices() override sealed;

    // Camera Stuff
    virtual inline void SetCameraLast(const float& mouseX, const float& mouseY) override sealed;
    //virtual inline glm::vec2 GetCameraLast() override sealed;
    virtual void UpdateCameraRotation(const float& mouseX, const float& mouseY) override sealed;
    virtual void UpdateCameraZoom(const float& mouseY) override sealed;

  private:

    bool fullscreen = false;
    bool m_wantRender = false;
    std::vector<IRenderable*> objects;
    std::vector<IRenderableText*> textObjects;
    //IRenderable* uiObj = nullptr;
    std::vector<IRenderable*> uiObj;
    Camera* mainCam;

  };


}