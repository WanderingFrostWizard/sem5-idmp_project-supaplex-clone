#include "Text.h"

using namespace GameView;

bool Text::Init(glm::mat4* projMatrix, int size)
{
  this->projMatrix = projMatrix;

  FT_Library ft;
  if (FT_Init_FreeType(&ft))
  {
    std::cout << "ERROR::FREETYPE: Could not init FreeType Library" << std::endl;
    return false;
  }


  FT_Face face;
  if (FT_New_Face(ft, Globals::MAIN_FONT, 0, &face))
  {
    std::cout << "ERROR::FREETYPE: Failed to load font" << std::endl;
    return false;
  }

  FT_Set_Pixel_Sizes(face, 0, size);

  if (FT_Load_Char(face, 'X', FT_LOAD_RENDER))
  {
    std::cout << "ERROR::FREETYTPE: Failed to load Glyph" << std::endl;
    return false;
  }

  glPixelStorei(GL_UNPACK_ALIGNMENT, 1); // Disable byte-alignment restriction

  for (GLubyte c = 0; c < 128; c++)
  {
    // Load character glyph 
    if (FT_Load_Char(face, c, FT_LOAD_RENDER))
    {
      std::cout << "ERROR::FREETYTPE: Failed to load Glyph" << std::endl;
      continue;
    }
    // Generate texture
    GLuint texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(
      GL_TEXTURE_2D,
      0,
      GL_RED,
      face->glyph->bitmap.width,
      face->glyph->bitmap.rows,
      0,
      GL_RED,
      GL_UNSIGNED_BYTE,
      face->glyph->bitmap.buffer
    );
    // Set texture options
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    // Now store character for later use
    Character character = {
      texture,
      glm::ivec2(face->glyph->bitmap.width, face->glyph->bitmap.rows),
      glm::ivec2(face->glyph->bitmap_left, face->glyph->bitmap_top),
      face->glyph->advance.x
    };
    Characters.insert(std::pair<GLchar, Character>(c, character));
  }

  FT_Done_Face(face);
  FT_Done_FreeType(ft);

  shader = new Shader("Data/Shaders/Text.vert", "Data/Shaders/Text.frag");

  glm::mat4 identity = glm::mat4(1.0);
  glm::mat4 projection = glm::ortho(0.0f, (float)Globals::WIN_WIDTH, 0.0f, (float)Globals::WIN_HEIGHT);

  UseShader(shader->id);
  glUniformMatrix4fv(shader->projMatrixLoc, 1, false, (float *)&projection);
  UseShader(0);

  unsigned * indices = new unsigned[6];
  indices[0] = 0;
  indices[1] = 1;
  indices[2] = 2;
  indices[3] = 0;
  indices[4] = 2;
  indices[5] = 3;

  // Generate Vertex Array Object
  glGenVertexArrays(1, &VAO);
  glBindVertexArray(VAO);

  // Generate and store Vertex Buffer Object
  glGenBuffers(1, &vertBuff);
  glBindBuffer(GL_ARRAY_BUFFER, vertBuff);
  glBufferData(GL_ARRAY_BUFFER, 4 * sizeof(Vertex), NULL, GL_DYNAMIC_DRAW);
  glGenBuffers(1, &indexBuff);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuff);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6 * sizeof(unsigned int), indices, GL_STATIC_DRAW);

  // Store data in Vertex Array Object
  glEnableVertexAttribArray(0);
  glEnableVertexAttribArray(1);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<void*>(0));
  glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<void*>(sizeof(glm::vec3)));

  // Unbind VBO and VAO
  glDisableVertexAttribArray(0);
  glDisableVertexAttribArray(1);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);

  delete indices;

  CHECK_GL_ERROR;

  return true;
}

void Text::Render(std::string text, GLfloat x, GLfloat y, GLfloat scale, glm::vec3 color)
{
  glDisable(GL_DEPTH_TEST);
  glDisable(GL_CULL_FACE);
  UseShader(shader->id);

  glUniform3f(glGetUniformLocation(shader->id, "textColor"), color.x, color.y, color.z);
  glActiveTexture(GL_TEXTURE0);
  glBindVertexArray(VAO);
  glEnableVertexAttribArray(0);
  glEnableVertexAttribArray(1);

  // Iterate through all characters
  std::string::const_iterator c;
  for (c = text.begin(); c != text.end(); c++)
  {
    Character ch = Characters[*c];

    GLfloat xpos = x + ch.Bearing.x * scale;
    GLfloat ypos = y - (ch.Size.y - ch.Bearing.y) * scale;

    GLfloat w = ch.Size.x * scale;
    GLfloat h = ch.Size.y * scale;

    // Update VBO for each character
    Vertex verts[4];
    verts[0].v = glm::vec3(xpos, ypos, 0.1f);
    verts[0].t = glm::vec2(0.0f, 1.0f);

    verts[1].v = glm::vec3(xpos + w, ypos, 0.1f);
    verts[1].t = glm::vec2(1.0f, 1.0f);

    verts[2].v = glm::vec3(xpos + w, ypos + h, 0.1f);
    verts[2].t = glm::vec2(1.0f, 0.0f);

    verts[3].v = glm::vec3(xpos, ypos + h, 0.1f);
    verts[3].t = glm::vec2(0.0f, 0.0f);

    // Render glyph texture over quad
    glBindTexture(GL_TEXTURE_2D, ch.TextureID);
    // Update content of VBO memory
    glBindBuffer(GL_ARRAY_BUFFER, vertBuff);
    glBufferSubData(GL_ARRAY_BUFFER, 0, 4 * sizeof(Vertex), verts);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // Render quad
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
    // Now advance cursors for next glyph (note that advance is number of 1/64 pixels)
    x += (ch.Advance >> 6) * scale; // Bitshift by 6 to get value in pixels (2^6 = 64)
  }

  glDisableVertexAttribArray(0);
  glDisableVertexAttribArray(1);
  glBindVertexArray(0);
  glBindTexture(GL_TEXTURE_2D, 0);
  UseShader(0);
  glEnable(GL_CULL_FACE);
  glEnable(GL_DEPTH_TEST);

  CHECK_GL_ERROR;
  
}