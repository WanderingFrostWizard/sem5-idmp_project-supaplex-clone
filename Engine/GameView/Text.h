#pragma once
#include <ft2build.h>
#include FT_FREETYPE_H
#include <map>
#include "../Includes.h"
#include "RenderingObjects.h"

namespace GameView
{

  struct Character {
    GLuint     TextureID;  // ID handle of the glyph texture
    glm::ivec2 Size;       // Size of glyph
    glm::ivec2 Bearing;    // Offset from baseline to left/top of glyph
    GLuint     Advance;    // Offset to advance to next glyph
  };

  class Text
  {
  private:

    std::map<GLchar, Character> Characters;
    glm::mat4* projMatrix = nullptr;

    GLuint VAO;
    Shader* shader = nullptr;

    GLuint vertBuff = -1;
    GLuint indexBuff = -1;

  public:

    Text() {}
    ~Text() { delete shader; }

    bool Init(glm::mat4* projMatrix, int size = 48);
    void Render(std::string text, GLfloat x, GLfloat y, GLfloat scale, glm::vec3 color);

  };

  //extern Text * textRenderer;

}