#include "Camera.h"
#include "../Utilities/Util.h"

using namespace GameView;

void Camera::FollowPlayer(const float& x, const float& y)
{
  camUpdateTimer += pIGame->GetDeltaTime();

  if(camUpdateTimer >= fixedDeltaTime)
  {
    camUpdateTimer = 0;

    glm::vec2 player(x, y);
    glm::vec2 camera(-m_pos.x, -m_pos.y);
    glm::vec2 difference = camera - player;

    if (difference.x < 0.4f && difference.x > -0.4f)
      difference.x = 0;
    if (difference.y < 0.2f && difference.y > -0.2f)
      difference.y = 0;

    //glm::vec2 delta = difference * fixedDeltaTime;
    glm::vec2 delta = difference * pIGame->GetDeltaTime();

    m_pos.x += delta.x;
    m_pos.y += delta.y;

  }

}

void Camera::ResetCamera(const float& x, const float& y)
{
  //fixedDeltaTime = pIGame->GetDeltaTime();
  m_pos = glm::vec3(-x, -y, 0.0f);
}

void Camera::Shake()
{
  shakeTimer = 1.0f;
}

glm::mat4 Camera::UpdateView()
{
  float x = 0;
  float y = 0;
  float z = 0;

  // Shake Shake Shake!
  if(shakeTimer > 0.0f)
  {
    x = (random.Random() - 0.5f) * shakeTimer * shakeScale;
    y = (random.Random() - 0.5f) * shakeTimer * shakeScale;
    z = (random.Random() - 0.5f) * shakeTimer * shakeScale;
    shakeTimer -= pIGame->GetDeltaTime();
  }

  glm::mat4 view(1.0f);
  view = glm::translate(view, glm::vec3(0.0f, 0.0f, m_zoom));
  view = glm::rotate(view, m_rot.y, glm::vec3(1.0f, 0.0f, 0.0f));
  view = glm::rotate(view, m_rot.x, glm::vec3(0.0f, 1.0f, 0.0f));
  view = glm::rotate(view, m_rot.z, glm::vec3(0.0f, 0.0f, 1.0f));
  view = glm::translate(view, glm::vec3(m_pos.x + x, m_pos.y + y, m_pos.z + z));
  return view;
}

glm::mat4 Camera::UpdatePerspective(int w, int h)
{
  //return glm::perspective<float>(m_fov, (float)w / (float)h, m_near, m_far);
  return glm::ortho<float>(-1.0f * (float)w / (float)h, 1.0f * (float)w / (float)h, -1.0f, 1.0f, m_near, m_far);
}

void Camera::UpdateRotation(const float& mouseX, const float& mouseY)
{
  float dX = mouseX - m_last.x;
  float dY = mouseY - m_last.y;

  m_rot.x += dX * 0.05f;
  m_rot.y += dY * 0.05f;
  m_rot.y = clamp(m_rot.y, glm::radians(-80.0f), glm::radians(80.0f));

  UpdateView();
}

void Camera::UpdateZoom(const float& mouseY)
{
  float dY = mouseY - m_last.y;
  m_zoom += dY * 0.01f;
  m_zoom = min(m_zoom, -0.5f);

  UpdateView();
}