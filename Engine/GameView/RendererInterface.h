#pragma once

#include "Shaders/Shaders.h"

namespace GameView
{
  class ICamera
  {
  public:
    virtual void FollowPlayer(const float& x, const float& y) = 0;
    virtual void ResetCamera(const float& x, const float& y) = 0;
    virtual void Shake() = 0;
  };

  class IRenderable
  {
  public:
    bool renderMe = true;
    int id = 0;

    IRenderable(bool render, int id) : renderMe(render), id(id) { };
    virtual ~IRenderable() { }

    virtual void Render() = 0;
  };

  class IRenderableText
  {
  public:
    bool renderMe = true;
    int id = 0;

    std::string text;
    float colour[3];
    float position[2];
    float size;

    IRenderableText(int id, std::string text, float size, float r, float g, float b, float x, float y) :
      id(id), text(text), size(size), colour{ r, g, b }, position{ x, y } {}
    virtual ~IRenderableText() { }
  };

  class IRenderer
  {
  public:
    // Levels
    Shader * titleShader;
    Shader * sparkleShader;
    Shader * lvlSelectShader;
    Shader * enterShader;
    Shader * backgroundShader;

    // Walls
    Shader * rockShader;
    Shader * breakableWallShader;

    // Objects
    Shader * playerShader;
    Shader * playerEatShader;
    Shader * dirtShader;
    Shader * darkDirtShader;
    Shader * crystalShader;
    Shader * crystalCleanShader;
    Shader * boulderShader;
    Shader * boulderCleanShader;
    Shader * exitShader;
    Shader * exitOpenShader;
    Shader * bombShader;
    Shader * bombCleanShader;

    // Enemies
    Shader * enemyShader;
    Shader * thiefShader;

    // UI
    Shader * uiShader;

    // Utilities
    Shader * explodeShader;

    virtual void Render() = 0;
    virtual void ReshapeWin(int w = 0, int h = 0) = 0;
    virtual void RequestRender() = 0;
    virtual bool WantsRender() = 0;

    // Renderable Object management
    virtual void RegisterObject(IRenderable* obj) = 0;
    virtual void UnregisterObject(IRenderable* obj) = 0;

    virtual void RegisterUI(IRenderable* obj) = 0;
    virtual void UnregisterUI(IRenderable* obj) = 0;

    virtual void RegisterText(IRenderableText* obj) = 0;
    virtual void UnregisterText(IRenderableText* obj) = 0;

    // Video Mode
    virtual void ToggleFullscreen() = 0;

    // Matrix Manipulation
    virtual inline void UpdateMatrices(Shader& shader) = 0;
    virtual inline void LoadIdentityMatrices() = 0;

    // Camera Stuff
    virtual void SetCameraLast(const float& mouseX, const float& mouseY) = 0;
    virtual void UpdateCameraRotation(const float& mouseX, const float& mouseY) = 0;
    virtual void UpdateCameraZoom(const float& mouseY) = 0;
  };
}