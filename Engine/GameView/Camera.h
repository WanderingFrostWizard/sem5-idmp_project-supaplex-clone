#pragma once
#include "../Includes.h"

namespace GameView
{

  class Camera : public ICamera
  {

  private:

    glm::vec3 m_pos;
    glm::vec3 m_rot;
    float m_zoom = -1;
    glm::vec2 m_last;
    float m_fov, m_near, m_far;
    float shakeTimer = 0.0f;
    const float shakeScale = 0.1f;
    
    float camUpdateTimer = 0.0f;
    const float fixedDeltaTime = 0.01666f;

  public:
    Camera() = delete;
    Camera(const glm::vec3& pos, const glm::vec3& rot, const float& zoom,
      const float& fov, const float& near, const float& far)
    : m_pos(pos), m_rot(rot), m_zoom(zoom), m_last(0.0f), m_fov(fov), m_near(near), m_far(far)
    {}
    ~Camera() {}

    glm::mat4 UpdateView();
    glm::mat4 UpdatePerspective(int w, int h);
    void UpdateRotation(const float& mouseX, const float& mouseY);
    void UpdateZoom(const float& mouseY);
    virtual void FollowPlayer(const float& x, const float& y) override;
    virtual void ResetCamera(const float& x, const float& y) override;
    virtual void Shake() override;

    inline void SetLast(const float& mouseX, const float& mouseY)
    {
      m_last.x = mouseX;
      m_last.y = mouseY;
    }

  };

}