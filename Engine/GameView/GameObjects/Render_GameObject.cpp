#include "../../GameModel/GameObjects/GameObject.h"
#include "../../GameModel/GameObjects/Utility Objects/TitleObject.h"
#include "../Renderer.h"

using namespace GameView;

void GameModel::GameObject::Render()
{

  UseShader(shader->id);

  // Manipulate Model View Matrix to position the object
  glm::mat4 newMatrix = glm::translate(identityMatrix, position);
  newMatrix = glm::rotate(newMatrix, rotation, glm::vec3(0.0f, 0.0f, 1.0f));
  switch(face)
  {
  case Direction::RIGHT:
    break;
  case Direction::LEFT:
    glCullFace(GL_FRONT);
    newMatrix = glm::scale(newMatrix, glm::vec3(-1.0f, 1.0f, 1.0f));
    break;
  case Direction::UP:
    newMatrix = glm::rotate(newMatrix, DEG_TO_RAD(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
    break;
  case Direction::DOWN:
    newMatrix = glm::rotate(newMatrix, DEG_TO_RAD(-90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
    break;
  }

  pIRenderer->UpdateMatrices(*shader);
  glUniformMatrix4fv(shader->modelMatrixLoc, 1, false, (float *)&newMatrix);
  
  glBindVertexArray(mainMesh->vao);
  glEnableVertexAttribArray(0);
  glEnableVertexAttribArray(1);

  glBindTexture(GL_TEXTURE_2D, shader->tex);
  glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

  glDisableVertexAttribArray(0);
  glDisableVertexAttribArray(1);
  glBindVertexArray(0);

  if(face == Direction::LEFT)
  {
    glCullFace(GL_BACK);
  }

  UseShader(0);

  CHECK_GL_ERROR;

}

void GameModel::TitleObject::Render()
{

  UseShader(shader->id);

  // Manipulate Model View Matrix to position the object
  glm::mat4 newMatrix = glm::translate(identityMatrix, position);
  newMatrix = glm::rotate(newMatrix, rotation, glm::vec3(0.0f, 0.0f, 1.0f));
  newMatrix = glm::scale(newMatrix, scale);

  //pIRenderer->UpdateMatrices(*shader);

  glUniformMatrix4fv(shader->modelMatrixLoc, 1, false, (float *)&newMatrix);
  glUniformMatrix4fv(shader->viewMatrixLoc, 1, false, (float *)&identityMatrix);

  glm::mat4 projection = glm::ortho(-1.77f, 1.77f, -1.0f, 1.0f);
  glUniformMatrix4fv(shader->projMatrixLoc, 1, false, (float *)&projection);

  //glUniformMatrix4fv(shader->viewMatrixLoc, 1, false, (float *)&identityMatrix);
  //glUniformMatrix4fv(shader->projMatrixLoc, 1, false, (float *)&projection);

  glBindVertexArray(mainMesh->vao);
  glEnableVertexAttribArray(0);
  glEnableVertexAttribArray(1);

  glBindTexture(GL_TEXTURE_2D, shader->tex);
  glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

  glDisableVertexAttribArray(0);
  glDisableVertexAttribArray(1);
  glBindVertexArray(0);

  if (face == Direction::LEFT)
  {
    glCullFace(GL_BACK);
  }

  UseShader(0);

  CHECK_GL_ERROR;

}