#version 450 core

uniform sampler2D text;
uniform vec3 textColor;

in vec2 uv;

void main(void)
{
  vec4 sampled = vec4(1.0, 1.0, 1.0, texture(text, uv).r);
  gl_FragColor = vec4(textColor, 1.0) * sampled;
}