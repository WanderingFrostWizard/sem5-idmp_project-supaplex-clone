#version 450 core

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projMatrix;

out mat4 finalMatrix;

layout(location = 0) in vec3 vert_xyz;
layout(location = 1) in vec2 vert_uv;

out vec2 uv;

void main(void)
{
  uv = vert_uv;
  finalMatrix = projMatrix * viewMatrix * modelMatrix;
  gl_Position = finalMatrix * vec4(vert_xyz, 1);
}