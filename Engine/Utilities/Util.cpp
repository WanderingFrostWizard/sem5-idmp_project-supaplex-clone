#include "Util.h"

glm::vec3 Location::ToWorld(Location& loc, int& boardWidth, int& boardHeight, glm::vec3 pos)
{
  glm::vec3 worldPos = glm::vec3(-(boardWidth / 2) * Globals::CELL_WIDTH + (Globals::CELL_WIDTH / 2.0f),
    (boardHeight / 2) * Globals::CELL_HEIGHT - (Globals::CELL_HEIGHT / 2.0f), 0.0f);
  worldPos += glm::vec3(Globals::CELL_WIDTH * loc.x, -(Globals::CELL_HEIGHT * loc.y), pos.z);

  return worldPos;
}

Direction TurnLeft(Direction dir)
{
  switch (dir)
  {
  case Direction::UP:
    return Direction::LEFT;
  case Direction::LEFT:
    return Direction::DOWN;
  case Direction::DOWN:
    return Direction::RIGHT;
  case Direction::RIGHT:
    return Direction::UP;
  }
}

Direction TurnRight(Direction dir)
{
  switch (dir)
  {
  case Direction::UP:
    return Direction::RIGHT;
  case Direction::LEFT:
    return Direction::UP;
  case Direction::DOWN:
    return Direction::LEFT;
  case Direction::RIGHT:
    return Direction::DOWN;
  }
}

Location GetLoc(Location loc, Direction movementDir)
{
  Location newLoc(-1, -1);
  switch (movementDir)
  {
  case Direction::UP:
    newLoc = Location(loc.x, loc.y - 1);
    break;
  case Direction::DOWN:
    newLoc = Location(loc.x, loc.y + 1);
    break;
  case Direction::LEFT:
    newLoc = Location(loc.x - 1, loc.y);
    break;
  case Direction::RIGHT:
    newLoc = Location(loc.x + 1, loc.y);
    break;
  }
  return newLoc;
}