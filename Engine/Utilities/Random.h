#pragma once
#include <climits>
#include <chrono>

#define DEFAULT_SEED 4357

#define N 624
#define M 397
#define MATRIX_A 0x9908b0df
#define UPPER_MASK 0x80000000
#define LOWER_MASK 0x7fffffff
#define TEMPERING_MASK_B 0x9d2c5680
#define TEMPERING_MASK_C 0xefc60000
#define TEMPERING_SHIFT_U(y)  (y >> 11)
#define TEMPERING_SHIFT_S(y)  (y << 7)
#define TEMPERING_SHIFT_T(y)  (y << 15)
#define TEMPERING_SHIFT_L(y)  (y >> 18)

namespace Utilities
{

  //========================================================================
  //
  //  "Mersenne Twister pseudorandom number generator" 
  //  Original Code written by Takuji Nishimura and Makoto Matsumoto
  //
  //========================================================================
  class RNG
  {
  private:

    unsigned int rseed;
    unsigned int rseed_sp;
    unsigned long mt[N];
    int mti;

  public:

    RNG() : rseed(1), rseed_sp(0), mti(N + 1)
    { SetRandomSeed(DEFAULT_SEED); }
    RNG(unsigned int startSeed) : rseed(1), rseed_sp(0), mti(N + 1)
    { SetRandomSeed(startSeed); }
    ~RNG() {}

    unsigned Random(unsigned int n);
    float Random();
    void SetRandomSeed(unsigned int n);
    unsigned GetRandomSeed();
    void Randomize();

  };


}