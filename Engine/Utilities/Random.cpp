#include "Random.h"
using namespace Utilities;

// GLOBAL RNG OBJECTS
RNG random;

// Returns a number from 0 to n (excluding n)
unsigned int RNG::Random(unsigned int n)
{
  unsigned long y;
  static unsigned long mag01[2] = { 0x0, MATRIX_A };

  if (n == 0)
    return(0);

  if (mti >= N) {
    int kk;

    if (mti == N + 1)
      SetRandomSeed(DEFAULT_SEED);

    for (kk = 0;kk<N - M;kk++) {
      y = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
      mt[kk] = mt[kk + M] ^ (y >> 1) ^ mag01[y & 0x1];
    }
    for (;kk<N - 1;kk++) {
      y = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
      mt[kk] = mt[kk + (M - N)] ^ (y >> 1) ^ mag01[y & 0x1];
    }
    y = (mt[N - 1] & UPPER_MASK) | (mt[0] & LOWER_MASK);
    mt[N - 1] = mt[M - 1] ^ (y >> 1) ^ mag01[y & 0x1];

    mti = 0;
  }

  y = mt[mti++];
  y ^= TEMPERING_SHIFT_U(y);
  y ^= TEMPERING_SHIFT_S(y) & TEMPERING_MASK_B;
  y ^= TEMPERING_SHIFT_T(y) & TEMPERING_MASK_C;
  y ^= TEMPERING_SHIFT_L(y);

  return (y%n);
}


float RNG::Random()
{
  float r = (float)Random(INT_MAX);
  float divisor = (float)INT_MAX;
  return (r / divisor);
}


void RNG::SetRandomSeed(unsigned int n)
{
  mt[0] = n & 0xffffffff;
  for (mti = 1; mti<N; mti++)
    mt[mti] = (69069 * mt[mti - 1]) & 0xffffffff;

  rseed = n;
}


unsigned int RNG::GetRandomSeed(void)
{
  return(rseed);
}


void RNG::Randomize(void)
{
  SetRandomSeed((unsigned int)time(NULL));
}