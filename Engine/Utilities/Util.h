#pragma once
#include "../Includes.h"

struct Location
{
  int x;
  int y;
  Location(int x, int y) : x(x), y(y) {}
  Location(Location& loc) : x(loc.x), y(loc.y) { }

  static glm::vec3 ToWorld(Location& loc, int& boardWidth, int& boardHeight, glm::vec3 pos = glm::vec3(0));
};

enum class ObjType
{
  STATIC, CASCADE, PLAYER, BOULDER, CRYSTAL, ENEMY, BOMB
};

enum class Direction
{
  RIGHT, LEFT, UP, DOWN
};

Direction TurnLeft(Direction dir);
Direction TurnRight(Direction dir);
Location GetLoc(Location loc, Direction movementDir);