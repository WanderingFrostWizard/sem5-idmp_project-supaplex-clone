#pragma once
#include "Random.h"
#include "../GameController/ControllerInterface.h"
#include "../GameModel/GameInterface.h"
#include "../GameView/RendererInterface.h"

#define min(a, b) (((a) < (b)) ? (a) : (b))
#define max(a, b) (((a) > (b)) ? (a) : (b))
#define clamp(x, a, b) min(max((x), (a)), (b))
#define DEG_TO_RAD(a) ( ( a * glm::pi<float>() ) / 180.0f )
#define RAD_TO_DEG(a) ( ( a * 180.0f ) / glm::pi<float>() )

// GLOBAL SETTINGS ETC...
struct SDL_Window;
namespace Globals
{
  // CONSTANTS
  static const char* GAME_TITLE = "Gemenon (Rockface Games)";

  static const int WIN_HEIGHT = 720;
  static const int WIN_WIDTH = 1280;
  static const float MILLI = 1000.0f;

  static const float CELL_WIDTH = 0.15f;
  static const float CELL_HEIGHT = 0.15f;

  static const char* STANDARDSHADER_VERT = "Data/Shaders/Standard.vert";
  static const char* STANDARDSHADER_FRAG = "Data/Shaders/Standard.frag";

  // Levels
  static const char* SPRITE_TITLE = "Data/Textures/Title.jpg";
  static const char* SPRITE_SPARKLE = "Data/Textures/sparkle.png";
  static const char* SPRITE_ENTER = "Data/Textures/enter.png";
  static const char* SPRITE_LVLSELECT = "Data/Textures/LevelSelect.jpg";
  static const char* SPRITE_BG = "Data/Textures/LevelBG.jpg";

  // Walls
  static const char* SPRITE_ROCK = "Data/Textures/Rock.png";
  static const char* SPRITE_BREAKABLE_WALL = "Data/Textures/BreakableWall.png";

  // Objects
  static const char* SPRITE_PLAYER = "Data/Textures/Rockface.png";
  static const char* SPRITE_PLAYER_EAT = "Data/Textures/Rockface_Eat.png";
  static const char* SPRITE_DIRT = "Data/Textures/Dirt.png";
  static const char* SPRITE_DARKDIRT = "Data/Textures/DarkDirt.png";
  static const char* SPRITE_CRYSTAL = "Data/Textures/Crystal.png";
  static const char* SPRITE_CRYSTAL_DIRT = "Data/Textures/Crystal_Dirt.png";
  static const char* SPRITE_BOULDER = "Data/Textures/Boulder.png";
  static const char* SPRITE_BOULDER_DIRT = "Data/Textures/Boulder_Dirt.png";
  static const char* SPRITE_EXIT = "Data/Textures/Exit.png";
  static const char* SPRITE_EXIT_OPEN = "Data/Textures/Exit_Open.png";
  static const char* SPRITE_BOMB = "Data/Textures/Bomb.png";
  static const char* SPRITE_BOMB_DIRT = "Data/Textures/Bomb_Dirt.png";

  // Enemies
  static const char* SPRITE_ENEMY = "Data/Textures/Enemy.png";
  static const char* SPRITE_THEIF = "Data/Textures/Thief.png";

  // UI
  static const char* SPRITE_UI = "Data/Textures/HUD.png";

  // Utilites
  static const char* SPRITE_EXPLODE = "Data/Textures/Explosion.png";

  static const char* MAIN_FONT = "Data/Fonts/consola.ttf";

  static const std::string NEWLEVELFILELOCATION = "Data/GemenonLevels.dat";
  static const std::string OLDLEVELFILELOCATION = "Data/LEVELS.dat";
  static const std::string SAVEFILELOCATION = "Data/SaveFile.txt";
  static const int TOTALNEWLEVELS = 30;
  static const int TOTALOLDLEVELS = 111;
  static const int LEVELNAMELENGTH = 23;

  struct LevelSettings
  {
    bool loadNewLevels = true;
    bool reloadLevels = false;
    int oldLevelsCompleted = 0;
    int newLevelsCompleted = 0;
  };
  extern LevelSettings levelSettings;

  static const float MOVE_SPEED = 4.0f;

  struct Window
  {
    SDL_Window* SDL_win;
    int width;
    int height;
  };
  extern Window window;

  struct PlayerControls
  {
    bool up = false;
    bool down = false;
    bool left = false;
    bool right = false;
    bool space = false;
    bool startGame = false;
    bool exitLevel = false;
  };

  extern PlayerControls playerControls;
}



// GAMEVIEW GLOBALS, SETTINGS ETC...
extern GLuint currentShader;

struct Debug
{
  bool report;
  bool errors;
};
extern Debug DEBUG;

// SUB SYSTEMS
extern GameController::IController* pIController;
extern GameView::IRenderer* pIRenderer;
extern GameView::ICamera* pICamera;
extern GameModel::IGame* pIGame;

// RANDOM NUMBER GENERATORS
extern Utilities::RNG random;