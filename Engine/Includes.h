#pragma once

// Third Party Libraries
#define GL_GLEXT_PROTOTYPES

#include <GL/glew.h>
#include <GL/glu.h>
#include <GL/gl.h>
#include <SDL2/SDL.h>

#define _USE_MATH_DEFINES
#define GLM_FORCE_RADIANS

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/constants.hpp>

// Internal Libraries
#include "Utilities/Globals.h"