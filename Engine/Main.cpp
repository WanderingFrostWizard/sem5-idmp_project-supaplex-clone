#include <cstdlib>
#include "Includes.h"
#include "GameController/Controller.h"
#include "GameModel/Game.h"
#include "GameView/Renderer.h"
#include <iostream>

Debug DEBUG = 
{
  false,  // general reporting (bound to 'r')
  false   // error reporting (bound to 'e')
};

Globals::Window Globals::window =
{
  nullptr,
  Globals::WIN_WIDTH,
  Globals::WIN_HEIGHT
};

Globals::LevelSettings Globals::levelSettings;

Globals::PlayerControls Globals::playerControls;

GameController::IController* pIController = nullptr;
GameView::IRenderer* pIRenderer = nullptr;
GameView::ICamera* pICamera = nullptr;
GameModel::IGame* pIGame = nullptr;

GameController::Controller* pController = nullptr;
GameView::Renderer* pRenderer = nullptr;
GameModel::Game* pGame = nullptr;

void Shutdown()
{
  // Delete MVC systems (ORDER IS IMPORTANT)
  delete pRenderer;
  delete pGame;
  delete pController;
}

int main(int argc, char *argv[])
{

  // Create MVC systems
  pGame = new GameModel::Game();
  pRenderer = new GameView::Renderer();
  pController = new GameController::Controller();

  // Store as Global Interface pointers so each system can access the others
  pIGame = pGame;
  pIRenderer = pRenderer;
  pIController = pController;

  // Initialize MVC systems (ORDER IS IMPORTANT)
  try
  {
    pController->Initialize();
    pRenderer->Initialize();
    pGame->Initialize();
  }
  catch(std::exception e)
  {
    std::cerr << e.what() << std::endl;
    Shutdown();
    return EXIT_FAILURE;
  }

  pController->RunGame();

  Shutdown();
  return EXIT_SUCCESS;
}