#include "InputManager.h"

using namespace GameController;
//using namespace glm;

bool InputManager::HandleEvents()
{

  SDL_Event event;

  while (SDL_PollEvent(&event))
  {
    switch (event.type)
    {
    case SDL_KEYUP:
      return HandleKeyboardUp(event.key.keysym.sym);
    case SDL_KEYDOWN:
      return HandleKeyboardDown(event.key.keysym.sym);
    case SDL_MOUSEBUTTONUP:
      HandleMouseUp(event.button);
      break;
    case SDL_MOUSEBUTTONDOWN:
      HandleMouseDown(event.button);
      break;
    case SDL_MOUSEMOTION:
      HandleMouseMotion(&event.motion);
      break;
    case SDL_WINDOWEVENT:
      switch (event.window.event)
      {
      case SDL_WINDOWEVENT_RESIZED:
        if (event.window.windowID == SDL_GetWindowID(Globals::window.SDL_win))
        {
          SDL_SetWindowSize(Globals::window.SDL_win, event.window.data1,
            event.window.data2);
          pIRenderer->ReshapeWin();
        }
        break;
      case SDL_WINDOWEVENT_CLOSE:
        return true;
      default:
        break;
      }
      break;
    default:
      break;
    }
  }

  return false;
}

bool InputManager::HandleKeyboardUp(SDL_Keycode& key)
{
  switch (key)
  {
    // MODIFIER_KEYS
  case SDLK_LSHIFT:
  case SDLK_RSHIFT:
    KEY_SHIFT = false;
    break;
  case SDLK_LALT:
  case SDLK_RALT:
    KEY_ALT = false;
    break;
  case SDLK_RETURN:
    KEY_ENTER = false;
    Globals::playerControls.startGame = false;

  // PLAYER CONTROLS
  case SDLK_UP:
    Globals::playerControls.up = false;
    break;
  case SDLK_DOWN:
    Globals::playerControls.down = false;
    break;
  case SDLK_LEFT:
    Globals::playerControls.left = false;
    break;
  case SDLK_RIGHT:
    Globals::playerControls.right = false;
    break;
  case SDLK_SPACE:
    Globals::playerControls.space = false;
    break;
  //case SDLK_ESCAPE:
  //  Globals::playerControls.exitLevel = false;
  //  break;
  default:
    break;
  }

  return false;
}

bool InputManager::HandleKeyboardDown(SDL_Keycode& key)
{
  switch (key)
  {
    // MODIFIER KEYS
  case SDLK_LSHIFT:
  case SDLK_RSHIFT:
    KEY_SHIFT = true;
    break;
  case SDLK_LALT:
  case SDLK_RALT:
    KEY_ALT = true;
    break;

    // IMMEDIATE COMMANDS
  case SDLK_q:
    return true;
  case SDLK_RETURN:
    if (KEY_ALT && !KEY_ENTER)
    {
      pIRenderer->ToggleFullscreen();
    } else
    {
      Globals::playerControls.startGame = true;
    }
    KEY_ENTER = true;
    break;
  case SDLK_e:
    DEBUG.errors = !DEBUG.errors;
    std::cout << "Debug (Error) Reporting: " << (DEBUG.errors ? "On" : "Off") << '\n';
    break;
  case SDLK_r:
    DEBUG.report = !DEBUG.report;
    std::cout << "Debug Reporting: " << (DEBUG.report ? "On" : "Off") << '\n';
    break;

  // PLAYER CONTROLS
  case SDLK_UP:
    Globals::playerControls.up = true;
    break;
  case SDLK_DOWN:
    Globals::playerControls.down = true;
    break;
  case SDLK_LEFT:
    Globals::playerControls.left = true;
    break;
  case SDLK_RIGHT:
    Globals::playerControls.right = true;
    break;
  case SDLK_SPACE:
    Globals::playerControls.space = true;
    break;
  case SDLK_ESCAPE:
    Globals::playerControls.exitLevel = true;
    break;
  default:
    break;
  }

  return false;
}

void InputManager::HandleMouseUp(SDL_MouseButtonEvent& mouse)
{
  if (mouse.button == SDL_BUTTON_LEFT)
    MOUSE_LMB = false;
  else if (mouse.button == SDL_BUTTON_RIGHT)
    MOUSE_RMB = false;
}

void InputManager::HandleMouseDown(SDL_MouseButtonEvent& mouse)
{
  pIRenderer->SetCameraLast(mouse.x, mouse.y);
  if (mouse.button == SDL_BUTTON_LEFT) {}
    //MOUSE_LMB = true;
  else if (mouse.button == SDL_BUTTON_RIGHT) {}
    //MOUSE_RMB = true;
}

void InputManager::HandleMouseMotion(SDL_MouseMotionEvent * mouse)
{
  if (MOUSE_LMB)
  {
    pIRenderer->UpdateCameraRotation(mouse->x, mouse->y);
  }

  if (MOUSE_RMB)
  {
    pIRenderer->UpdateCameraZoom(mouse->y);
  }

  pIRenderer->SetCameraLast(mouse->x, mouse->y);
}