#include "Controller.h"

using namespace GameController;

Controller::Controller() {}

Controller::~Controller()
{

  for (auto manager : m_eventManagers)
  {
    delete manager;
  }

  SDL_DestroyWindow(Globals::window.SDL_win);
  SDL_Quit();

}


void Controller::Initialize()
{

  if (!InitSDL())
  {
    std::string error = "Error initialising SDL2, aborting program.\n";
    error.append(SDL_GetError());
    throw std::runtime_error(error);
  }

  Factory_EventManagers();

}


bool Controller::InitSDL()
{

  if (SDL_Init(SDL_INIT_VIDEO) < 0) {
    return false;
  }

  SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
  SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
  SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
  SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
  SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
  SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

  Globals::window.SDL_win = SDL_CreateWindow(Globals::GAME_TITLE,
    SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, Globals::WIN_WIDTH, Globals::WIN_HEIGHT,
    SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
  if (!Globals::window.SDL_win) {
    return false;
  }

  // Everything initialised correctly
  return true;

}


void Controller::Factory_EventManagers()
{

  InputManager* inputMan = new InputManager();
  m_eventManagers.push_back(inputMan);

}


void Controller::RunGame()
{

  bool run = true;
  while (run)
  {
    // update the state of the game
    pIGame->Update();

    // Render to the screen, only if instructed to on the last loop
    if (pIRenderer->WantsRender())
    {
      pIRenderer->Render();
    }

    // Handle all events and end program if any returns true
    for (auto manager : m_eventManagers)
    {
      if (manager->HandleEvents())
      {
        run = false;
        break;
      }
    }
  }

}