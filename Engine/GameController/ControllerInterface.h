#pragma once

namespace GameController
{

  class IController
  {
  };

  class IEventsManager
  {
  public:
    virtual bool HandleEvents() = 0;
  };

}