#pragma once
#include <stdexcept>
#include <vector>
#include "../Includes.h"
#include "InputManager.h"

namespace GameController
{

  class Controller : public IController
  {
  public:

    Controller();
    ~Controller();

    void Initialize();
    void Factory_EventManagers();

    void RunGame();

  private:

    std::vector<IEventsManager*> m_eventManagers;

    bool InitSDL();

  };

}