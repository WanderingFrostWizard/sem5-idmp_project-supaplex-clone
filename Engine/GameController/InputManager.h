#pragma once
#include "../Includes.h"
#include "Controller.h"
#include <iostream>

namespace GameController
{


  class InputManager : public IEventsManager
  {

  public:
    virtual bool HandleEvents() override sealed;

  private:

    bool KEY_ALT = false;
    bool KEY_SHIFT = false;
    bool KEY_ENTER = false;
    bool MOUSE_LMB = false;
    bool MOUSE_RMB = false;

    bool HandleKeyboardUp(SDL_Keycode& key);
    bool HandleKeyboardDown(SDL_Keycode& key);
    void HandleMouseUp(SDL_MouseButtonEvent& mouse);
    void HandleMouseDown(SDL_MouseButtonEvent& mouse);
    void HandleMouseMotion(SDL_MouseMotionEvent * mouse);

  };


}