var class_game_view_1_1_i_renderer =
[
    [ "LoadIdentityMatrices", "class_game_view_1_1_i_renderer.html#a3ac5623d25dc0add1308a6ecdf3f4c0d", null ],
    [ "RegisterObject", "class_game_view_1_1_i_renderer.html#aefeadbb22c4f3451f45e28b962c666ab", null ],
    [ "Render", "class_game_view_1_1_i_renderer.html#a06c10084d9ffa034b4278c76bbb71b55", null ],
    [ "RequestRender", "class_game_view_1_1_i_renderer.html#a3ae574772c50ce4f1137c077424cf241", null ],
    [ "ReshapeWin", "class_game_view_1_1_i_renderer.html#a8d85f7f303851ed5daf8675a5f0b509e", null ],
    [ "SetCameraLast", "class_game_view_1_1_i_renderer.html#a94ac43ddd7bc765fdd36e0932ced1eb1", null ],
    [ "ToggleFullscreen", "class_game_view_1_1_i_renderer.html#a7f66adcc8d84c95ab6af5eadeef618b6", null ],
    [ "UpdateCameraRotation", "class_game_view_1_1_i_renderer.html#a3e950520be3dd4c5b273d47c08652582", null ],
    [ "UpdateCameraZoom", "class_game_view_1_1_i_renderer.html#a2393920823212148899a946979202566", null ],
    [ "UpdateMatrices", "class_game_view_1_1_i_renderer.html#aa32bbc5c142c82bb21de4e7e9049a7e4", null ],
    [ "WantsRender", "class_game_view_1_1_i_renderer.html#a6eb8160a736a97ae4c5d414a3deea692", null ]
];