var hierarchy =
[
    [ "GameView::Camera", "class_game_view_1_1_camera.html", null ],
    [ "Debug", "struct_debug.html", null ],
    [ "GameController::IController", "class_game_controller_1_1_i_controller.html", [
      [ "GameController::Controller", "class_game_controller_1_1_controller.html", null ]
    ] ],
    [ "GameController::IEventsManager", "class_game_controller_1_1_i_events_manager.html", [
      [ "GameController::InputManager", "class_game_controller_1_1_input_manager.html", null ]
    ] ],
    [ "GameModel::IGame", "class_game_model_1_1_i_game.html", [
      [ "GameModel::Game", "class_game_model_1_1_game.html", null ]
    ] ],
    [ "GameModel::IGameObject", "class_game_model_1_1_i_game_object.html", null ],
    [ "GameView::IRenderable", "class_game_view_1_1_i_renderable.html", null ],
    [ "GameView::IRenderer", "class_game_view_1_1_i_renderer.html", [
      [ "GameView::Renderer", "class_game_view_1_1_renderer.html", null ]
    ] ],
    [ "Utilities::RNG", "class_utilities_1_1_r_n_g.html", null ],
    [ "Shader", "class_shader.html", null ],
    [ "Globals::Window", "struct_globals_1_1_window.html", null ]
];