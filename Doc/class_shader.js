var class_shader =
[
    [ "Update", "class_shader.html#ab561bde180e5a07aebfa06873cc90719", null ],
    [ "id", "class_shader.html#afe360c94e9f0727889b198f773cff5e2", null ],
    [ "modelMatrixLoc", "class_shader.html#a790269523dce643d8bb828a7f4bb98c7", null ],
    [ "projMatrixLoc", "class_shader.html#a19cb6c29cd55170c56dcf177f71b5779", null ],
    [ "references", "class_shader.html#a3cdabfa40a3c0dd0dbb8df6394681c9e", null ],
    [ "viewMatrixLoc", "class_shader.html#a8a6a321b7077129c9e3091bcf47d59c7", null ]
];