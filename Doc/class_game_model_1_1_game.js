var class_game_model_1_1_game =
[
    [ "Game", "class_game_model_1_1_game.html#ac271e8a9d07d44281ec0a6de5157ec44", null ],
    [ "~Game", "class_game_model_1_1_game.html#ae3d112ca6e0e55150d2fdbc704474530", null ],
    [ "CalcTime", "class_game_model_1_1_game.html#a978bb6e98521b9f6b930c6fef74fc386", null ],
    [ "GetDeltaTime", "class_game_model_1_1_game.html#a1f23f9461c204b67b817014ebee3c645", null ],
    [ "Initialize", "class_game_model_1_1_game.html#adc01a7fae5261c95f7e6b41024e6c533", null ],
    [ "Update", "class_game_model_1_1_game.html#a336b0707e47b0e12c737c11f4dc60b4e", null ]
];