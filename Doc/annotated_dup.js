var annotated_dup =
[
    [ "GameController", null, [
      [ "Controller", "class_game_controller_1_1_controller.html", "class_game_controller_1_1_controller" ],
      [ "IController", "class_game_controller_1_1_i_controller.html", null ],
      [ "IEventsManager", "class_game_controller_1_1_i_events_manager.html", "class_game_controller_1_1_i_events_manager" ],
      [ "InputManager", "class_game_controller_1_1_input_manager.html", "class_game_controller_1_1_input_manager" ]
    ] ],
    [ "GameModel", null, [
      [ "Game", "class_game_model_1_1_game.html", "class_game_model_1_1_game" ],
      [ "IGame", "class_game_model_1_1_i_game.html", "class_game_model_1_1_i_game" ],
      [ "IGameObject", "class_game_model_1_1_i_game_object.html", "class_game_model_1_1_i_game_object" ]
    ] ],
    [ "GameView", null, [
      [ "Camera", "class_game_view_1_1_camera.html", "class_game_view_1_1_camera" ],
      [ "IRenderable", "class_game_view_1_1_i_renderable.html", "class_game_view_1_1_i_renderable" ],
      [ "IRenderer", "class_game_view_1_1_i_renderer.html", "class_game_view_1_1_i_renderer" ],
      [ "Renderer", "class_game_view_1_1_renderer.html", "class_game_view_1_1_renderer" ]
    ] ],
    [ "Globals", null, [
      [ "Window", "struct_globals_1_1_window.html", "struct_globals_1_1_window" ]
    ] ],
    [ "Utilities", null, [
      [ "RNG", "class_utilities_1_1_r_n_g.html", "class_utilities_1_1_r_n_g" ]
    ] ],
    [ "Debug", "struct_debug.html", "struct_debug" ],
    [ "Shader", "class_shader.html", "class_shader" ]
];